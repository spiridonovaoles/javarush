package com.javarush.test.level38.lesson04.task02;

/* Непроверяемые исключения (unchecked exception)
Напиши реализацию метода methodThrowsClassCastException(). Он должен
всегда кидать непроверяемое исключение ClassCastException.

Напиши реализацию метода methodThrowsNullPointerException(). Он должен
всегда кидать непроверяемое исключение NullPointerException.

Кинуть исключение (throw) явно нельзя.
*/

import java.util.*;

public class VeryComplexClass {
    public void methodThrowsClassCastException() {
        List list = new ArrayList();
        list.add("abc");
        list.add(new Integer(5)); //OK

        for(Object obj : list){
            String str = (String) obj; //здесь приведение типов бросит ClassCastException
        }
    }

    public void methodThrowsNullPointerException() {
        Map map = new TreeMap();
        map.put(null,  "null");
        System.out.println(map.get(null));
    }

}
