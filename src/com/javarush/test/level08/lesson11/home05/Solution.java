package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/* Мама Мыла Раму. Теперь с большой буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Вывести результат на экран.

Пример ввода:
  мама     мыла раму.

Пример вывода:
  Мама     Мыла Раму.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        char buf[] = s.toCharArray();
        buf[0] = Character.toUpperCase(buf[0]);
int n = buf.length;
            for (int i = 1; i < n; i++)
            {

                if (Character.isLetter(buf[i]) && buf[i - 1] == ' ')
                {
                    buf[i] = Character.toUpperCase(buf[i]);
                }
            }
        for(char ch: buf )
        {

            System.out.print(ch);
        }

    }


}
