package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;
import java.util.Arrays;

public class Solution
{
    public static void main(String[] args)
    {
        Human child1 = new Human("Aby", false, 2,new ArrayList<Human>());
        Human child2 = new Human("Ali", false, 3,new ArrayList<Human>());
        Human child3 = new Human("Fonc", true, 5,new ArrayList<Human>());
        Human pa = new Human("John", true, 35, new ArrayList<Human>(Arrays.asList(child1, child2, child3)));
        Human ma = new Human("Tane", false, 28, new ArrayList<Human>(Arrays.asList(child1, child2, child3)));
        Human grandpa1 = new Human("Tom", true, 84, new ArrayList<Human>(Arrays.asList(ma)));
        Human grandpa2 = new Human("Jack", true, 88,new ArrayList<Human>(Arrays.asList(pa)));
        Human grandma1 = new Human("Jane", false, 86,new ArrayList<Human>(Arrays.asList(ma)));
        Human grandma2 = new Human("Ann", false, 85,new ArrayList<Human>(Arrays.asList(pa)));

        System.out.println(grandpa1.toString());
        System.out.println(grandpa2.toString());
        System.out.println(grandma1.toString());
        System.out.println(grandma2.toString());
        System.out.println(pa.toString());
        System.out.println(ma.toString());
        System.out.println(child1.toString());
        System.out.println(child2.toString());
        System.out.println(child3.toString());
    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;

        public Human(String name, boolean sex, int age, ArrayList<Human> children)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
