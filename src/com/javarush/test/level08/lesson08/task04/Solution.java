package com.javarush.test.level08.lesson08.task04;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Сталлоне", new Date("JUNE 1 1980"));
        map.put("Феллини", new Date("JANUARY 13 1934"));
        map.put("Гудини", new Date("JULY 14 1980"));
        map.put("Буанинни", new Date("MAY 15 1945"));
        map.put("Стиллонне", new Date("JUNE 24 1960"));
        map.put("Стилллаве", new Date("MARCH 13 1970"));
        map.put("Столыпин", new Date("AUGUST 12 1940"));
        map.put("Уиллис", new Date("DECEMBER 14 1986"));
        map.put("Шварц", new Date("OCTOBER 16 1998"));
        map.put("Сигал", new Date("NOVEMBER 12 1933"));
        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        Iterator<Map.Entry<String, Date>> itr = map.entrySet().iterator();
        while(itr.hasNext()) {
            Map.Entry<String, Date>pair=itr.next();
            //String key = pair.getKey();
            Date value = pair.getValue();
            int x = value.getMonth();
            if( x == 5 || x == 6 || x == 7 )
            {itr.remove();}

        }

    }
}
