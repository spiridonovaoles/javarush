package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        Set <String> list = new HashSet<String>();
        list.add("лето");
        list.add("лыко");
        list.add("лак");
        list.add("лесть");
        list.add("лыжи");
        list.add("лень");
        list.add("лепка");
        list.add("листья");
        list.add("липа");
        list.add("лор");
        list.add("лось");
        list.add("лол");
        list.add("ловкость");
        list.add("лес");
        list.add("лиса");
        list.add("лира");
        list.add("лев");
        list.add("лис");
        list.add("лист");
        list.add("лоб");
        return (HashSet)list;
    }
}
