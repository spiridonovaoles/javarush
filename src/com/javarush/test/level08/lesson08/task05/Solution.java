package com.javarush.test.level08.lesson08.task05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> book = new HashMap<String, String>();
        book.put("Иванова", "Анна");
        book.put("Петрова", "Алла");
        book.put("Сидорова", "Ольга");
        book.put("Тихончева", "Полина");
        book.put("Панасюк", "Ульяна");
        book.put("Скворцова", "Рита");
        book.put("Щеглова", "Анна");
        book.put("Петровская", "Дарья");
        book.put("Ивановская", "Зина");
        book.put("Ломовая", "Бамбино");
        return book;
    }

    public static void main(String[] args)
    {
        HashMap<String, String> book = createMap();
        removeTheFirstNameDuplicates(book);
        //System.out.println(book.values());
    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)

    {
            ArrayList<String> list = new ArrayList<String>(map.values());
            HashSet<String> set = new HashSet<String>();

            for(String s:list)
            {
                if(set.contains(s))
                    removeItemFromMapByValue(map,s);
                else set.add(s);
            }
        }


    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}
