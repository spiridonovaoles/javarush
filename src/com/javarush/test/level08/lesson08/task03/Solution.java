package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        Map<String, String> book = new HashMap<String, String>();
        book.put("Иванова", "Анна");
        book.put("Петрова", "Алла");
        book.put("Сидорова", "Ольга");
        book.put("Тихончева", "Полина");
        book.put("Панасюк", "Ульяна");
        book.put("Скворцова", "Рита");
        book.put("Щеглова", "Анна");
        book.put("Петровская", "Дарья");
        book.put("Ивановская", "Зина");
        book.put("Ломовая", "Бамбино");
        return (HashMap) book;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        Iterator<Map.Entry<String, String>> itr = map.entrySet().iterator();
        int i = 0;
        while(itr.hasNext()) {
            Map.Entry<String, String>pair=itr.next();
            String value =pair.getValue();
            if (name.equals(value))
            {
                i++;
            }
        }
        return i;

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
        Iterator<Map.Entry<String, String>> itr = map.entrySet().iterator();
        int i2 = 0;
        while(itr.hasNext()) {
            Map.Entry<String, String>pair=itr.next();
            String key =pair.getKey();
            if (familiya.equals(key)){
                i2++;
            }
        }
        return i2;

    }
}
