package com.javarush.test.level33.lesson15.big01;

import com.javarush.test.level33.lesson15.big01.strategies.StorageStrategy;
import com.javarush.test.level33.lesson15.big01.tests.SpeedTest;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Olesya on 17.01.2016.
 */
public class Solution
{
    public static Set<Long> getIds(Shortener shortener, Set<String> strings)
    {
        Set<Long> set = new HashSet<>();
        for (String st : strings)
        {
            set.add(shortener.getId(st));
        }
        return set;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys)
    {
        Set<String> set = new HashSet<>();
        for (Long lg : keys)
        {
            set.add(shortener.getString(lg));
        }
        return set;
    }

    public static void testStrategy(StorageStrategy strategy, long elementsNumber)
    {
        Helper.printMessage(strategy.getClass().getSimpleName());

        Set<String> testStrings = new HashSet<>();
        for (long i = 0; i < elementsNumber; i++) {
            testStrings.add(Helper.generateRandomString());
        }

        Shortener shortener = new Shortener(strategy);

        Long t1 = new Date().getTime();
        Set<Long> keySet = getIds(shortener, testStrings);
        Long t2 = new Date().getTime();

        Long duration1 = t2 - t1;
        Helper.printMessage(duration1.toString());
        Long t3 = new Date().getTime();
        Set<String> stringSet = getStrings(shortener, keySet);
        Long t4 = new Date().getTime();
        Long duration2 = t4 - t3;
        Helper.printMessage(duration2.toString());

        if (stringSet.equals(testStrings)) {
            Helper.printMessage("Тест пройден.");
        } else {
            Helper.printMessage("Тест не пройден.");
        }
    }

    public static void main(String[] args)
    {
/*        StorageStrategy strategy = new HashMapStorageStrategy();
        testStrategy(strategy, 10000L);
        StorageStrategy strategy2 = new OurHashMapStorageStrategy();
        testStrategy(strategy2, 10000L);
        StorageStrategy strategy3 = new FileStorageStrategy();
        testStrategy(strategy3, 100L);
        StorageStrategy strategy4 = new OurHashBiMapStorageStrategy();
        testStrategy(strategy4, 10000L);
        StorageStrategy strategy5 = new HashBiMapStorageStrategy();
        testStrategy(strategy5, 10000L);
        StorageStrategy strategy6 = new DualHashBidiMapStorageStrategy();
        testStrategy(strategy6, 10000L);*/
        JUnitCore runner = new JUnitCore();
        Result result = runner.run(SpeedTest.class);
        for (Failure failure : result.getFailures())
        {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
}
