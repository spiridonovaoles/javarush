package com.javarush.test.level33.lesson15.big01.tests;

import com.javarush.test.level33.lesson15.big01.Helper;
import com.javarush.test.level33.lesson15.big01.Shortener;
import com.javarush.test.level33.lesson15.big01.strategies.HashBiMapStorageStrategy;
import com.javarush.test.level33.lesson15.big01.strategies.HashMapStorageStrategy;
import com.javarush.test.level33.lesson15.big01.strategies.StorageStrategy;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Olesya on 27.01.2016.
 */
public class SpeedTest
{
    public long getTimeForGettingIds(Shortener shortener, Set<String> strings, Set<Long> ids){
        long t1 = new Date().getTime();
        for(String st: strings) {
            ids.add(shortener.getId(st));
        }
        long t2 = new Date().getTime();
        long duration = t2 - t1;
        return duration;
    }

    public long getTimeForGettingStrings(Shortener shortener, Set<Long> ids, Set<String> strings) {
        long t1 = new Date().getTime();
        for(Long id: ids) {
            strings.add(shortener.getString(id));
        }
        long t2 = new Date().getTime();
        long duration = t2 - t1;
        return duration;
    }

    @Test
    public void testHashMapStorage(){
        StorageStrategy strategy1 = new HashMapStorageStrategy();
        Shortener shortener1 = new Shortener(strategy1);
        StorageStrategy strategy2 = new HashBiMapStorageStrategy();
        Shortener shortener2 = new Shortener(strategy2);
        Set<String> origStrings = new HashSet<>();
        for(long i = 0 ; i < 10000; i++)
        {
            origStrings.add(Helper.generateRandomString());
        }
        Set<Long> ids = new HashSet<>();
        long t1 = getTimeForGettingIds(shortener1, origStrings, ids);
        Set<Long> ids2 = new HashSet<>();
        long t2 = getTimeForGettingIds(shortener2, origStrings, ids2);
        Assert.assertTrue(t1>t2);
        Assert.assertEquals(getTimeForGettingStrings(shortener1, ids2, origStrings), getTimeForGettingStrings(shortener2,ids2,origStrings),5);
    }


}
