package com.javarush.test.level39.lesson09.big01;

import com.javarush.test.level39.lesson09.big01.query.IPQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class LogParser implements IPQuery
{

    private final Path logDir;

    public LogParser(Path logDir)
    {
        this.logDir = logDir;
    }

    @Override
    public int getNumberOfUniqueIPs(Date after, Date before)
    {
        Set<String> ips = new HashSet();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm:ss");
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(logDir))
        {
            for (Path file : directoryStream)
            {
                if (file.toString().toLowerCase().endsWith(".log"))
                {
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(Files.newInputStream(file, StandardOpenOption.READ))))
                    {
                        while (reader.ready())
                        {
                            String line = reader.readLine();
                            String[] parts = line.split("\\t");
                            if (parts.length > 4)
                            {
                                String ip = parts[0];
                                String time = parts[2];
                                //if (parts[1].equalsIgnoreCase(user))
                                //{
                                Date date = dateFormat.parse(time);
                                if (before != null && after != null)
                                {
                                    if (date.getTime() >= after.getTime() && date.getTime() <= before.getTime())
                                    {
                                        ips.add(ip);
                                    }
                                } else if (before != null)
                                {
                                    if (date.getTime() <= before.getTime())
                                    {
                                        ips.add(ip);
                                    }
                                } else if (after != null)
                                {
                                    if (date.getTime() >= after.getTime())
                                    {
                                        ips.add(ip);
                                    }
                                } else
                                {
                                    ips.add(ip);
                                }
                                // }
                            }
                        }
                    }
                    catch (ParseException e)
                    {
                    }
                }
            }
        }
        catch (IOException e)
        {
        }
        return 0;
    }

    @Override
    public Set<String> getUniqueIPs(Date after, Date before)
    {
        return null;
    }

    @Override
    public Set<String> getIPsForUser(String user, Date after, Date before)
    {

        Set<String> ips = new HashSet();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm:ss");
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(logDir))
        {
            for (Path file : directoryStream)
            {
                if (file.toString().toLowerCase().endsWith(".log"))
                {
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(Files.newInputStream(file, StandardOpenOption.READ))))
                    {
                        while (reader.ready())
                        {
                            String line = reader.readLine();
                            String[] parts = line.split("\\t");
                            if (parts.length > 4)
                            {
                                String ip = parts[0];
                                String time = parts[2];
                                if (parts[1].equalsIgnoreCase(user))
                                {
                                    Date date = dateFormat.parse(time);
                                    if (before != null && after != null)
                                    {
                                        if (date.getTime() >= after.getTime() && date.getTime() <= before.getTime())
                                        {
                                            ips.add(ip);
                                        }
                                    } else if (before != null)
                                    {
                                        if (date.getTime() <= before.getTime())
                                        {
                                            ips.add(ip);
                                        }
                                    } else if (after != null)
                                    {
                                        if (date.getTime() >= after.getTime())
                                        {
                                            ips.add(ip);
                                        }
                                    } else
                                    {
                                        ips.add(ip);
                                    }
                                }
                            }
                        }
                    }
                    catch (ParseException e)
                    {
                    }
                }
            }
        }
        catch (IOException e)
        {
        }
        return ips;
    }

    @Override
    public Set<String> getIPsForEvent(Event event, Date after, Date before)
    {
        return null;
    }

    @Override
    public Set<String> getIPsForStatus(Status status, Date after, Date before)
    {
        return null;
    }

}