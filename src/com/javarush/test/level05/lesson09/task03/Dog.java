package com.javarush.test.level05.lesson09.task03;

/* Создать класс Dog
Создать класс Dog (собака) с тремя конструкторами:
- Имя
- Имя, рост
- Имя, рост, цвет
*/

public class Dog
{
    private String friendname = null;
    private int friendage = 0;
    private String friendcolor = null;

    public  Dog(String name){
        this.friendname = name;
    }
    public  Dog(Dog name, int age){
        this.friendage = age;
    }
    public  Dog(Dog name, Dog age,String color){
        this.friendcolor = color;
    }


}
