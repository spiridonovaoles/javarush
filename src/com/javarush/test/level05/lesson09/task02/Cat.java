package com.javarush.test.level05.lesson09.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью конструкторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст – неизвестные. Кот - бездомный)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес не известен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    private String catname = null;
    private int catage = 0;
    private int catweight = 0;
    private String catcolor = null;
    private String cataddress = null;

    public Cat(String name){
        this.catname = name;
        this.catage = 5;
        this.catweight = 6;
    }
    public Cat(String name, int age, int weight)
    {
        this.catname = name;
        this.catage = age;
        this.catweight = weight;
    }

    public Cat(String name, int age)
    {
        this.catname = name;
        this.catage = age;
        this.catweight = 20;
    }
    public Cat(int weight, String color)
    {
        this.catcolor = color;
        this.catweight = weight;
        this.catname = "noname";
        this.cataddress = "noadr";
    }
    public Cat(int weight, String color,String address)
    {
        this.catcolor = color;
        this.catweight = weight;
        this.catname = "noname";
        this.cataddress = address;
    }//Напишите тут ваш код

}
