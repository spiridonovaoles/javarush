package com.javarush.test.level05.lesson07.task01;

/* Создать класс Friend
Создать класс Friend (друг) с тремя инициализаторами (тремя методами initialize):
- Имя
- Имя, возраст
- Имя, возраст, пол
*/

public class Friend
{
    private String friendname = null;
    private int friendage = 0;
    private String friendgender = null;

    public void initialize(String name){
        this.friendname = name;
    }
    public void initialize(Friend name, int age){
        this.friendage = age;
    }
    public void initialize(Friend name, Friend age,String gender){
        this.friendgender = gender;
    }
}
