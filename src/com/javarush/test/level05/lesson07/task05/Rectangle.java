package com.javarush.test.level05.lesson07.task05;

/* Создать класс прямоугольник (Rectangle)
Создать класс прямоугольник (Rectangle). Его данными будут top, left, width, height (левая координата, верхняя, ширина и высота). Создать для него как можно больше методов initialize(…)
Примеры:
-	заданы 4 параметра: left, top, width, height
-	ширина/высота не задана (оба равны 0)
-	высота не задана (равно ширине) создаём квадрат
-	создаём копию другого прямоугольника (он и передаётся в параметрах)
*/

public class Rectangle
{
    private int left = 0;
    private int top = 0;
    private int vidth = 0;
    private int height = 0;

    public void initialize(int x, int y){
        this.left = x;
        this.top = y;

    }
    public void initialize(Rectangle x, Rectangle y, int vidth){
        this.vidth = vidth;
    }
    public void initialize(Rectangle x, Rectangle y,  Rectangle vidth, int height){
        this.height = height;
    }
    public void initialize(Rectangle x, Rectangle y){
        this.height = 0;
        this.vidth = 0;
    }
    public void initialize(Rectangle x, Rectangle y, Rectangle vidth){
        this.height = this.vidth;
    }



}
