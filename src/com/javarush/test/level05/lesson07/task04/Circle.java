package com.javarush.test.level05.lesson07.task04;

/* Создать класс Circle
Создать класс (Circle) круг, с тремя инициализаторами:
- centerX centerY, radius
- centerX, centerY, radius, width
- centerX, centerY, radius, width, color
*/

public class Circle
{
    private int centerX = 0;
    private int centerY = 0;
    private int radius = 0;
    private int vidth = 0;
    private String color = null;

    public void initialize(int x, int y, int r){
        this.centerX = x;
        this.centerY = y;
        this.radius = r;

    }
    public void initialize(Circle centerX, Circle centerY, Circle radius, int vidth){
        this.vidth = vidth;
    }
    public void initialize(Circle centerX, Circle centerY, Circle radius, Circle vidth, String color){
        this.color = color;
    }

}
