package com.javarush.test.level05.lesson07.task03;

/* Создать класс Dog
Создать класс Dog (собака) с тремя инициализаторами:
- Имя
- Имя, рост
- Имя, рост, цвет
*/

public class Dog
{
    private String friendname = null;
    private int friendage = 0;
    private String friendcolor = null;

    public void initialize(String name){
        this.friendname = name;
    }
    public void initialize(Dog name, int age){
        this.friendage = age;
    }
    public void initialize(Dog name, Dog age,String color){
        this.friendcolor = color;
    }

}
