package com.javarush.test.level05.lesson07.task02;

/* Создать класс Cat
Создать класс Cat (кот) с пятью инициализаторами:
- Имя,
- Имя, вес, возраст
- Имя, возраст (вес стандартный)
- вес, цвет, (имя, адрес и возраст неизвестны, это бездомный кот)
- вес, цвет, адрес ( чужой домашний кот)
Задача инициализатора – сделать объект валидным. Например, если вес неизвестен, то нужно указать какой-нибудь средний вес. Кот не может ничего не весить. То же касательно возраста. А вот имени может и не быть (null). То же касается адреса: null.
*/

public class Cat
{
    private String catname = null;
    private int catage = 0;
    private int catweight = 0;
    private String catcolor = null;
    private String cataddress = null;

    public void initialize(String name){
        this.catname = name;
        this.catage = 5;
        this.catweight = 6;
    }
    public void initialize(String name, int age, int weight)
    {
        this.catname = name;
        this.catage = age;
        this.catweight = weight;
    }

    public void initialize(String name, int age)
    {
        this.catname = name;
        this.catage = age;
        this.catweight = 20;
    }
    public void initialize(int weight, String color)
    {
        this.catcolor = color;
        this.catweight = weight;
        this.catname = "noname";
        this.cataddress = "noadr";
    }
    public void initialize(int weight, String color,String address)
    {
        this.catcolor = color;
        this.catweight = weight;
        this.catname = "noname";
        this.cataddress = address;
    }

}
