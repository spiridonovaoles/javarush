package com.javarush.test.level05.lesson05.task02;

/* Реализовать метод fight
Реализовать метод boolean fight(Cat anotherCat):
реализовать механизм драки котов в зависимости от их веса, возраста и силы.
Зависимость придумать самому. Метод должен определять, выиграли ли мы (this) бой или нет,
т.е. возвращать true, если выиграли и false - если нет.
Должно выполняться условие:
если cat1.fight(cat2) = true , то cat2.fight(cat1) = false
*/

public class Cat
{
    public String name;
    public int age;
    public int weight;
    public int strength;

    public Cat()
    {
//        this.name = name;
//        this.age = age;
//        this.weight = weight;
//        this.strength = strength;
    }
    /*public static void main(String[] args)
    {
        Cat cat1 = new Cat("Tom", 7,10,10);
        Cat cat2 = new Cat("Kom", 5,11,9);
        cat1.strength = cat1.age + cat1.weight +cat1.strength;
        cat2.strength= cat2.age + cat2.weight + cat2.strength;

        System.out.println(cat1.fight(cat2));
    }
*/
    public boolean fight (Cat anotherCat)
    {
        return (this.strength > anotherCat.strength);

    }
}
