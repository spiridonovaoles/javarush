package com.javarush.test.level23.lesson04.task01;

/* Inner
Реализовать метод getTwoSolutions, который должен возвращать массив из 2-х экземпляров класса Solution.
Для каждого экземпляра класса Solution инициализировать поле innerClasses двумя значениями.
Инициализация всех данных должна происходить только в методе getTwoSolutions.
*/
public class Solution {
    public InnerClass[] innerClasses = new InnerClass[2];

    public class InnerClass {
    }

    public static Solution[] getTwoSolutions() {
        Solution[] sol = new Solution[2];
        Solution inst1 = new Solution();
        inst1.innerClasses[0]=inst1.new InnerClass();
        inst1.innerClasses[1]=inst1.new InnerClass();
        sol[0] = inst1;
        Solution inst2 = new Solution();
        inst2.innerClasses[0]=inst2.new InnerClass();
        inst2.innerClasses[1]=inst2.new InnerClass();
        sol[1] = inst2;
        return sol;
    }
}
