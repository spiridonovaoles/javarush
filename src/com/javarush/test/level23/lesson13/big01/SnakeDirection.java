package com.javarush.test.level23.lesson13.big01;

/**
 * Created by Olesya on 20.08.2015.
 */
public enum SnakeDirection
{
    UP,
    RIGHT,
    DOWN,
    LEFT
}
