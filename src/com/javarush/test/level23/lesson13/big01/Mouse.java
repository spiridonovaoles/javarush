package com.javarush.test.level23.lesson13.big01;

/**
 * Created by Olesya on 20.08.2015.
 */
public class Mouse
{
    public int x;
    public int y;

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public Mouse(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
