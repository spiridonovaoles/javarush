package com.javarush.test.level07.lesson06.task01;

/* 5 различных строчек в списке
1. Создай список строк.
2. Добавь в него 5 различных строчек.
3. Выведи его размер на экран.
4. Используя цикл выведи его содержимое на экран, каждое значение с новой строки.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        String[] str = {"a", "asas" , "asasa" , "fgfg", "gftrh"};
       // BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      //  for (int i = 0; i < str.length; i++)
       // {
       //     str[i] = reader.readLine();
       // }

        System.out.println(str.length);
        for (int i = 0; i < str.length; i++)
        {
            System.out.println(str[i]);
        }
        //Напишите тут ваш код

    }
}
