package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        Human human = new Human("Вера", false, 75, null, null);
        Human human2 = new Human("Петр", true, 74,null, null);
        Human human3 = new Human("Варвара", false, 86, null, null);
        Human human4 = new Human("Григорий", true, 87, null, null);
        Human human5 = new Human("Катя", false, 65, human2, human);
        Human human6 = new Human("Павел", true, 64, human4, human3);
        Human human7 = new Human("Наталья", false, 32, human6, human5);
        Human human8= new Human("Артем", true, 35, human6, human5);
        Human human9 = new Human("Иван", true, 13, human6, human5);
    }

    public static class Human
    {
        String name;
        Boolean sex;
        int age;
        Human father;
        Human mother;
public Human(String name, Boolean sex, int age, Human father, Human mother){
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.father = father;
        this.mother = mother;
    System.out.println(toString());
}
        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
