package com.javarush.test.level19.lesson10.home01;

/* Считаем зарплаты
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Все данные вывести в консоль, предварительно отсортировав в возрастающем порядке по имени
Закрыть потоки

Пример входного файла:
Петров 2
Сидоров 6
Иванов 1.35
Петров 3.1

Пример вывода:
Иванов 1.35
Петров 5.1
Сидоров 6.0
*/

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader inputStream = new BufferedReader(new FileReader(args[0]));
        Map<String, Double> map = new TreeMap<String, Double>();
        String st, str = new String();
        while ((st = inputStream.readLine()) != null) str += st + "\n";
        str= str.replaceAll("\n", " ");
        String[] ss= str.split(" ");
        for (int i = 0; i < ss.length-1; i+=2)
        {
            String name = ss[i];
            double count = Double.parseDouble(ss[i + 1]);
            int c = 0;

            for (Map.Entry<String, Double> pair : map.entrySet()) {
                if (ss[i].equals(pair.getKey())) {
                    count += pair.getValue();
                    c++;
                    pair.setValue(count);
                }
            }
            if (c == 0) map.put(name, count);
        }
        for (Map.Entry<String, Double> pair : map.entrySet())
        {
            System.out.println(pair.getKey() + "  " + pair.getValue());
        }

        inputStream.close();
    }
}
