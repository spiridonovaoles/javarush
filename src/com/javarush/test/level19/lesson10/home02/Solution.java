package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args)throws IOException{
        BufferedReader inputStream = new BufferedReader(new FileReader(args[0]));
        Map<String, Double> map = new TreeMap<String, Double>();
        String st, str = new String();
        while ((st = inputStream.readLine()) != null) str += st + "\n";
        str= str.replaceAll("\n", " ");
        String[] ss= str.split(" ");
        for (int i = 0; i < ss.length-1; i+=2)
        {
            String SymbolIn = ss[i];
            double count = Double.parseDouble(ss[i + 1]);
            int c = 0;

            for (Map.Entry<String, Double> pair : map.entrySet()) {
                if (ss[i].equals(pair.getKey())) {
                    count += pair.getValue();
                    c++;
                    pair.setValue(count);
                }
            }
            if (c == 0) map.put(SymbolIn, count);
        }
        double max = 0;
        ArrayList<String> res = new ArrayList<String>();
        for (Map.Entry<String, Double> pair : map.entrySet())
        {
            if(pair.getValue()>max)max = pair.getValue();
        }
        for (Map.Entry<String, Double> pair : map.entrySet())
        {
            if(max ==pair.getValue())res.add(pair.getKey());
        }

        for(String s: res) System.out.println(s);
        inputStream.close();
    }
}
