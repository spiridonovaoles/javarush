package com.javarush.test.level19.lesson10.home07;

/* Длинные слова
В метод main первым параметром приходит имя файла1, вторым - файла2
Файл1 содержит слова, разделенные пробелом.
Записать через запятую в Файл2 слова, длина которых строго больше 6
Закрыть потоки

Пример выходных данных:
длинное,короткое,аббревиатура
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Solution
{
    public static void main(String[] args) throws IOException
    {

        BufferedReader inputStream = new BufferedReader(new FileReader(args[0]));
        FileWriter outputStream = new FileWriter(args[1]);
        String st, str = new String();
        String res = "";
        List<String> list = new ArrayList<String>();
        while ((st = inputStream.readLine()) != null)
            str += st + " ";
        String[] ss = str.split(" ");
        for (int i = 0; i < ss.length; i++)
        {
            char[] ch = ss[i].toCharArray();

            if (ch.length > 6)
            {
                list.add(ss[i]);
            }
        }
        for (int i = 0; i < list.size() - 1; i++)
        {
            res += list.get(i) + ",";
        }
        res = res + list.get(list.size()-1);
        outputStream.write(res);
        inputStream.close();
        outputStream.close();
    }

}
