package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит слова, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {

        BufferedReader inputStream = new BufferedReader(new FileReader(args[0]));
        FileWriter outputStream = new FileWriter(args[1]);
        String st;

        while ((st = inputStream.readLine()) != null)
        {
            String res = "";
            String[] str = st.split(" ");
            for (String s : str)
            {
                char[] ch = s.toCharArray();
                int count = 0;
                for (char c : ch)
                {
                    if (Character.isDigit(c)) count++;
                }
                if (count != 0) res += s + " ";
            }
            outputStream.write(res);
        }
        inputStream.close();
        outputStream.close();
    }
}
