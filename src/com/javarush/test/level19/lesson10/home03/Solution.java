package com.javarush.test.level19.lesson10.home03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;


/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution
{
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader inputStream = new BufferedReader(new FileReader(args[0]));
        String st, str, dateStr = new String();
        while ((st = inputStream.readLine()) != null)
        {
            char[] ch = st.toCharArray();
            int i = 0;
            while (!Character.isDigit(ch[i]))
            {
                i++;
            }
            str = st.substring(0, i - 1);
            dateStr = st.substring(i, ch.length);
            String[] dd = dateStr.split(" ");
            Date birth =new Date();
            birth.setDate(Integer.parseInt(dd[0]));
            birth.setMonth(Integer.parseInt(dd[1])-1);
            birth.setYear(Integer.parseInt(dd[2])-1900);
            PEOPLE.add(new Person(str, birth));
        }

        inputStream.close();
    }

}
