package com.javarush.test.level19.lesson10.bonus01;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/* Отслеживаем изменения
Считать в консоли 2 имени файла - file1, file2.
Файлы содержат строки, file2 является обновленной версией file1, часть строк совпадают.
Нужно создать объединенную версию строк, записать их в список lines
Операции ADDED и REMOVED не могут идти подряд, они всегда разделены SAME
Пример:
[Файл 1]
строка1
строка2
строка3
[Файл 2]
строка1
строка3
строка4
[Результат - список lines]
SAME строка1
REMOVED строка2
SAME строка3
ADDED строка4
*/
public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader brf1 = new BufferedReader(new FileReader(reader.readLine()));
        BufferedReader brf2 = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();
        String list1One = brf1.readLine();
        String list1Two = brf1.readLine();
        String list2One = brf2.readLine();
        String list2Two = brf2.readLine();
        while (list1One != null || list2One != null) {
            if (list1One != null && list2One == null) {
                lines.add(new LineItem(Type.REMOVED, list1One));
                list1One = list1Two;
                list1Two = brf1.readLine();
            }
            else if (list1One == null) {
                lines.add(new LineItem(Type.ADDED, list2One));
                list2One = list2Two;
                list2Two = brf2.readLine();
            }
            else {
                if (list1One.equals(list2One)) {
                    lines.add(new LineItem(Type.SAME, list1One));
                    list1One = list1Two;
                    list1Two = brf1.readLine();
                    list2One = list2Two;
                    list2Two = brf2.readLine();
                } else if (list1One.equals(list2Two)) {
                    lines.add(new LineItem(Type.ADDED, list2One));
                    list2One = list2Two;
                    list2Two = brf2.readLine();
                } else if (list1Two.equals(list2One)) {
                    lines.add(new LineItem(Type.REMOVED, list1One));
                    list1One = list1Two;
                    list1Two = brf1.readLine();
                }
            }
        }
        brf1.close();
        brf2.close();
    }
    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }
    public static class LineItem {
        public Type type;
        public String line;
        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}

