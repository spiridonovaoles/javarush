package com.javarush.test.level19.lesson10.home08;

/* Перевертыши
1 Считать с консоли имя файла.
2 Для каждой строки в файле:
2.1 переставить все символы в обратном порядке
2.2 вывести на экран
3 Закрыть поток

Пример тела входного файла:
я - программист.
Амиго

Пример результата:
.тсиммаргорп - я
огимА
*/

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader inputStream = new BufferedReader(new FileReader(reader.readLine()));
        String st;

        List<String> list = new ArrayList();
        while ((st = inputStream.readLine()) != null)
        {
            String res = "";
            String[] ss = st.split(" ");
            for (int i = ss.length - 1; i >= 0; i--)
            {
                char[] ch = ss[i].toCharArray();
                String ch_rev = "";
                for (int j = 0; j < ch.length; j++)
                {
                    ch_rev += ch[ch.length - j - 1];
                }
               res+=ch_rev + " ";
            }
            list.add(res);
        }
        for (int i = 0; i < list.size(); i++)
        {
            System.out.println(list.get(i).trim());
        }
        inputStream.close();
    }
}
