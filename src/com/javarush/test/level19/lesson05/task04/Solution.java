package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки ввода-вывода.
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        BufferedReader inputStream = new BufferedReader(new FileReader(file1));
        FileWriter outputStream = new FileWriter(file2);
        String st, str = new String();
        while ((st = inputStream.readLine()) != null) str += st + "\n";
        String ss = "";
        for (int i = 0; i < str.length() ; i++)
        {
            char charAt = str.charAt(i);
            if(charAt == '.') charAt = '!';
            ss += charAt;
        }
        outputStream.write(ss);
        outputStream.close();
        reader.close();
        inputStream.close();
    }
}
