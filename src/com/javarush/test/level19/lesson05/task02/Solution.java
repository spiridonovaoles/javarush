package com.javarush.test.level19.lesson05.task02;

/* Считаем слово
Считать с консоли имя файла.
Вывести в консоль количество слов "world", которые встречаются в файле.
Закрыть поток ввода.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader readerFile = new BufferedReader(new FileReader(reader.readLine()));
        reader.close();
        String st, str = new String();
        while((st = readerFile.readLine())!= null)
            str += st + "\n";
        readerFile.close();
        int count = 0;
        str = str.replaceAll("\\p{P}", " ");
        String[] array1 = str.split("\n");
        for (String s : array1)
        {
            String[] array2 = s.split(" ");
            for (String s2 : array2)
            {
                if (s2.equals("world"))
                    count++;
            }
        }
        System.out.println(count);


    }

}
