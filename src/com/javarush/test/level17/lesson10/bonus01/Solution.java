package com.javarush.test.level17.lesson10.bonus01;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Locale;


/* CRUD
CrUD - Create, Update, Delete
Программа запускается с одним из следующих наборов параметров:
-c name sex bd
-u id name sex bd
-d id
-i id
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-с  - добавляет человека с заданными параметрами в конец allPeople, выводит id (index) на экран
-u  - обновляет данные человека с данным id
-d  - производит логическое удаление человека с id
-i  - выводит на экран информацию о человеке с id: name sex (м/ж) bd (формат 15-Apr-1990)

id соответствует индексу в списке
Все люди должны храниться в allPeople
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat

Пример параметров: -c Миронов м 15/04/1990  Петров м 23/01/1990 Сидоров м 11/01/1989
*/

public class Solution
{
    public static List<Person> allPeople = new ArrayList<Person>();
    static String name;
    static char sex;
    static int id;
    static SimpleDateFormat sfIn;
    static SimpleDateFormat sf;

    static
    {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException
    {
        sfIn = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        sf = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        if (args.length > 0)
        {
            if (args[0].equals("-c"))
            {
                create(args);
            } else if (args[0].equals("-u"))
            {
                update(args);
            } else if (args[0].equals("-d"))
            {
                delete(args);
            }
            else if (args[0].equals("-i"))
            {
                info(args);
            }
        }
    }


    static void create(String... args) throws ParseException
    {

        name = args[1];
        Date bd = sfIn.parse(args[3]);
        if(args[2].equals("м")){
            allPeople.add(Person.createMale(name, bd));
        }
        else if(args[2].equals("ж")){
            allPeople.add(Person.createFemale(name, bd));
        }
            System.out.println(allPeople.size() - 1);
    }

    static void update(String... args) throws ParseException
    {
        id = Integer.parseInt(args[1]);
        name = args[2];
        Date bd = sfIn.parse(args[4]);
        if(args[3].equals("м")){
            allPeople.set(id, Person.createMale(name, bd));
        }
        else if(args[3].equals("ж")){
            allPeople.set(id, Person.createFemale(name, bd));
        }
    }

    static void delete(String... args) throws ParseException
    {
        id = Integer.parseInt(args[1]);
        Person nomen = allPeople.get(id);
        nomen.setBirthDay(null);
        nomen.setName(null);
        nomen.setSex(null);
    }

    static void info(String... args) throws ParseException
    {
        id = Integer.parseInt(args[1]);
        Person men = allPeople.get(id);
        System.out.println(men.getName() + " " + convert(men.getSex()) + " " + sf.format(men.getBirthDay()));
    }
    public static String convert(Sex sex)
    {
        if (sex == Sex.MALE)
        {
            return "м";
        } else
        {
            return "ж";
        }
    }

}

