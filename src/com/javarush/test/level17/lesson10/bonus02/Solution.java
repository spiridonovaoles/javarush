package com.javarush.test.level17.lesson10.bonus02;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* CRUD 2
CrUD Batch - multiple Creation, Updates, Deletion
!!!РЕКОМЕНДУЕТСЯ выполнить level17.lesson10.bonus01 перед этой задачей!!!

Программа запускается с одним из следующих наборов параметров:
-c name1 sex1 bd1 name2 sex2 bd2 ...
-u id1 name1 sex1 bd1 id2 name2 sex2 bd2 ...
-d id1 id2 id3 id4 ...
-i id1 id2 id3 id4 ...
Значения параметров:
name - имя, String
sex - пол, "м" или "ж", одна буква
bd - дата рождения в следующем формате 15/04/1990
-с  - добавляет всех людей с заданными параметрами в конец allPeople, выводит id (index) на экран в соответствующем порядке
-u  - обновляет соответствующие данные людей с заданными id
-d  - производит логическое удаление всех людей с заданными id
-i  - выводит на экран информацию о всех людях с заданными id: name sex bd

id соответствует индексу в списке
Формат вывода даты рождения 15-Apr-1990
Все люди должны храниться в allPeople
Порядок вывода данных соответствует вводу данных
Обеспечить корректную работу с данными для множества нитей (чтоб не было затирания данных)
Используйте Locale.ENGLISH в качестве второго параметра для SimpleDateFormat -u 0 Миронов м 15/04/1990 1 Петров м 23/01/1990 Сидоров м 11/01/1989
*/


public class Solution
{
    static String name;
    static int id;
    static SimpleDateFormat sfIn;
    static SimpleDateFormat sf;
    public static List<Person> allPeople = new ArrayList<Person>();

    static
    {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException
    {
        sfIn = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        sf = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        if (args[0].equals("-c"))
        {
            for (int i = 0; i < args.length - 3; i = i + 3)
            {
                create(i, args);
            }
        } else if (args[0].equals("-u"))
        {
            for (int i = 0; i < args.length - 4; i += 4)
            {
                update(i, args);
            }
        } else if (args[0].equals("-d"))
        {
            for (int i = 0; i < args.length-1; i++)
            {
                delete(i, args);
            }
        } else if (args[0].equals("-i"))
        {
            for (int i = 0; i < args.length-1; i++)
            {
                info(i, args);
            }
        }

    }


    static void create(int i, String... args) throws ParseException
    {

        name = args[1 + i];
        Date bd = sfIn.parse(args[3 + i]);
        if (args[2 + i].equals("м"))
        {
            allPeople.add(Person.createMale(name, bd));
        } else if (args[2 + i].equals("ж"))
        {
            allPeople.add(Person.createFemale(name, bd));
        }
        System.out.println(allPeople.size() - 1);
    }

    static synchronized void update(int i, String... args) throws ParseException
    {
        id = Integer.parseInt(args[1+i]);
        name = args[2+i];
        Date bd = sfIn.parse(args[4+i]);
        if (args[3+i].equals("м"))
        {
            allPeople.set(id, Person.createMale(name, bd));
        } else if (args[3+i].equals("ж"))
        {
            allPeople.set(id, Person.createFemale(name, bd));
        }
    }

    static synchronized void delete(int i, String... args) throws ParseException
    {
        id = Integer.parseInt(args[1+i]);
        Person nomen = allPeople.get(id);
        nomen.setBirthDay(null);
        nomen.setName(null);
        nomen.setSex(null);
    }

    static synchronized void info(int i, String... args) throws ParseException
    {
        id = Integer.parseInt(args[1+i]);
        Person men = allPeople.get(id);
        System.out.println(men.getName() + " " + convert(men.getSex()) + " " + sf.format(men.getBirthDay()));
    }

    public static String convert(Sex sex)
    {
        if (sex == Sex.MALE)
        {
            return "м";
        } else
        {
            return "ж";
        }
    }

}

