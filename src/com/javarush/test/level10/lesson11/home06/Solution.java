package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        String name;
        int age;
        boolean sex;
        int child;
        String building;
        int flat;
        Human(String name){
            this.name = name;
        }
        Human(String name, boolean sex){
            this.name = name;
            this.sex = sex;
        }
        Human(String name, boolean sex, int age){
            this.name = name;
            this.sex = sex;
            this.age = age;
        }
        Human(String name, boolean sex, int age,int child){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.child = child;
        }
        Human(String name, boolean sex, int age,int child, String building){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.child = child;
            this.building = building;
        }
        Human(String name, boolean sex, int age,int child, String building,int flat){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.child = child;
            this.building = building;
            this.flat = flat;
        }
        Human(String name, boolean sex, String building){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.child = child;
            this.building = building;
            this.flat = flat;
        }
        Human(String name, boolean sex,int child, String building,int flat){
            this.name = name;
            this.sex = sex;
            this.child = child;
            this.building = building;
            this.flat = flat;
        }
        Human(String name, int age,int child, String building,int flat){
            this.name = name;
            this.age = age;
            this.child = child;
            this.building = building;
            this.flat = flat;
        }
        Human(String name, boolean sex, int age, String building){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.building = building;

        }

    }
}
