package com.javarush.test.level18.lesson03.task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;



/* Самые частые байты
Ввести с консоли имя файла
Найти байты, которые чаше всех встречаются в файле
Вывести их на экран через пробел.
Закрыть поток ввода-вывода
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        List<Integer> byteList = new ArrayList<Integer>();
        Map<Integer,Integer> count = new HashMap<Integer,Integer>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream readln = new FileInputStream(reader.readLine());
        while (readln.available() > 0)
        {
            int data = readln.read();
            byteList.add(data);
        }
        for (int i = 0; i < byteList.size(); i++)
        {
            count.put(byteList.get(i), Collections.frequency(byteList, byteList.get(i)));
        }
        List<Integer> valueList = new ArrayList<Integer>(count.values());
        Object[] a = valueList.toArray();
        Arrays.sort(a);
        for (Map.Entry<Integer, Integer>pair: count.entrySet())
        {
            if (a[a.length - 1].equals(pair.getValue()))
            {
                    System.out.print(pair.getKey() + " ");

            }
        }
        readln.close();
        reader.close();
    }
}
