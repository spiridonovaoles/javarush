package com.javarush.test.level18.lesson03.task05;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* Сортировка байт
Ввести с консоли имя файла
Считать все байты из файла.
Не учитывая повторений - отсортировать их по байт-коду в возрастающем порядке.
Вывести на экран
Закрыть поток ввода-вывода

Пример байт входного файла
44 83 44

Пример вывода
44 83
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        List<Integer> byteList = new ArrayList<Integer>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream readln = new FileInputStream(reader.readLine());
        while (readln.available() > 0)
        {
            int data = readln.read();
            byteList.add(data);
        }
        List<Integer> duplicates = new ArrayList<Integer>();
        for (Integer ii: byteList)
        {
            if(duplicates.contains(ii)){
                continue;
            }
            else{duplicates.add(ii);}
        }
        Object[] a = duplicates.toArray();
        Arrays.sort(a);
        for (Object obj : a)
        {
            System.out.print(obj + " ");

        }
        readln.close();
        reader.close();
    }
}
