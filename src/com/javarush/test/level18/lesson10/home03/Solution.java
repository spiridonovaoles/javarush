package com.javarush.test.level18.lesson10.home03;

/* Два в одном
Считать с консоли 3 имени файла
Записать в первый файл содержимого второго файла, а потом дописать содержимое третьего файла
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException{

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileOutputStream outputStream = new FileOutputStream(reader.readLine());
        String file2 = reader.readLine();
        FileInputStream inputStream = new FileInputStream(file2);
        String file3 = reader.readLine();
        FileInputStream inputStream2 = new FileInputStream(file3);
        int count1 = inputStream.available();
        int count2 = inputStream2.available();
        byte[] buffer1 = new byte[count1];
        byte[] buffer2 = new byte[count2];
        int count = inputStream.read(buffer1);
        outputStream.write(buffer1, 0, count);
        count = inputStream2.read(buffer2);
        outputStream.write(buffer2, 0, count);
        outputStream.close();
        reader.close();
        inputStream.close();
        inputStream2.close();
    }
}
