package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть все потоки ввода-вывода
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;
import java.util.Set;
import java.util.TreeSet;


public class Solution
{
    public static void main(String[] args) throws IOException
    {
        Set<String> list = new TreeSet();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String st;
        String name = "";
        while (true)
        {
            st = reader.readLine();
            if (!st.equals("end"))
            {
                name = st.substring(0, st.lastIndexOf(46));
                list.add(st.substring(st.lastIndexOf(46) + 1, st.length()));
            } else break;
        }
        File myFile = new File(name);
        myFile.createNewFile();
        FileOutputStream fileOut = new FileOutputStream(name);
        for (String i : list)
        {
            FileInputStream fileIn = new FileInputStream(name+"."+i);
            byte []bytes = new byte[fileIn.available()];
            fileIn.read(bytes);
            fileOut.write(bytes);
            fileIn.close();
        }
        fileOut.close();
        reader.close();

    }
}
