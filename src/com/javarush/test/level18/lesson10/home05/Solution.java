package com.javarush.test.level18.lesson10.home05;

/* Округление чисел
Считать с консоли 2 имени файла
Первый файл содержит вещественные(дробные) числа, разделенные пробелом. Например, 3.1415
Округлить числа до целых и записать во второй файл
Закрыть потоки
Принцип округления:
3.49 - 3
3.50 - 4
3.51 - 4
*/

import java.io.*;
import java.lang.Math;
import java.util.regex.Pattern;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        BufferedReader readerFile_1 = new BufferedReader(new FileReader(file1));
        FileWriter outputStream = new FileWriter(file2);
        String str = "";
        while (true)
        {
            String data = readerFile_1.readLine();
            if (data == null) break;
            str += data;
        }
        Pattern pat = Pattern.compile(" ");
        String[] str_mass = pat.split(str);
        String n = "";
        for (int i = 0; i < str_mass.length; i++)
        {
            n += Math.round(new Float(str_mass[i])) + " ";
        }
        outputStream.write(n);
        outputStream.close();
        reader.close();
        readerFile_1.close();
    }
}
