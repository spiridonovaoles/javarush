package com.javarush.test.level18.lesson10.home06;

/* Встречаемость символов
Программа запускается с одним параметром - именем файла, который содержит английский текст.
Посчитать частоту встречания каждого символа.
Отсортировать результат по возрастанию кода ASCII (почитать в инете). Пример: ','=44, 's'=115, 't'=116
Вывести на консоль отсортированный результат:
[символ1]  частота1
[символ2]  частота2
Закрыть потоки

Пример вывода:
, 19
- 7
f 361
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        FileReader reader = new FileReader(args[0]);
        Map<Character, Integer> numChars = new TreeMap<Character, Integer>();
        while (reader.ready()) {
            int SymbolIn = reader.read();
            int count = 1;
            for (Map.Entry<Character, Integer> pair : numChars.entrySet()) {
                if (SymbolIn == pair.getKey()) {
                    count = count + pair.getValue();
                }
            }
            numChars.put((char)SymbolIn, count);
        }
            for (Map.Entry<Character, Integer> pair : numChars.entrySet())
            {
                    System.out.println(pair.getKey() + "  " + pair.getValue());
            }
        reader.close();
    }
}
