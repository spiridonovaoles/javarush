package com.javarush.test.level18.lesson10.home08;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


/* Нити и байты
Читайте с консоли имена файлов, пока не будет введено слово "exit"
Передайте имя файла в нить ReadThread
Нить ReadThread должна найти байт, который встречается в файле максимальное число раз, и добавить его в словарь resultMap,
где параметр String - это имя файла, параметр Integer - это искомый байт.
Не забудьте закрыть все потоки
*/

public class Solution
{
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String st = "";
        while (!st.equals("exit"))
        {
            st = reader.readLine();
            Thread thread = new ReadThread(st);
            thread.start();
        }
        reader.close();
    }

    public static class ReadThread extends Thread
    {
        public ReadThread(String fileName)
        {
            super(fileName);
        }
        public void run()
        {
            try
            {
                List<Integer> byteList = new ArrayList<Integer>();
                Map<Integer, Integer> count = new HashMap<>();
                FileInputStream readln = new FileInputStream(getName());
                while (readln.available() > 0)
                {
                    int data = readln.read();
                    byteList.add(data);
                }
                for (int i = 0; i < byteList.size(); i++)
                {
                    count.put(byteList.get(i), Collections.frequency(byteList, byteList.get(i)));
                }
                Set<Map.Entry<Integer, Integer>> set = count.entrySet();
                int max = 0;
                for (Map.Entry<Integer, Integer> pair : set)
                {
                    if (pair.getValue() > max) max = pair.getValue();
                }
                for (Map.Entry<Integer, Integer> pair : set)
                {
                    if (pair.getValue().equals(max)) max = pair.getKey();
                }
                resultMap.put(getName(), max);
                readln.close();
            }
            catch (IOException e)
            {
            }
        }
    }
}
