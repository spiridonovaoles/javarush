package com.javarush.test.level18.lesson10.home02;

/* Пробелы
В метод main первым параметром приходит имя файла.
Вывести на экран частоту встречания пробела. Например, 10.45
1. Посчитать количество всех символов.
2. Посчитать количество пробелов.
3. Вывести на экран п2/п1*100, округлив до 2 знаков после запятой
Закрыть потоки
*/


import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;


public class Solution
{
    public static void main(String[] args) throws  IOException
    {
        FileInputStream fileInputStream = new FileInputStream(args[0]);
        int[] buff = new int[fileInputStream.available()];
        if (fileInputStream.available()>0)
        {
            for (int i = 0; i < buff.length; i++)
            {
                int data = fileInputStream.read();
                buff[i] = data;
            }
        }
        int count = 0;
        for (int i = 0; i < buff.length; i++)
        {
            if( buff[i] == 32){
                count ++;
            }
        }
        Double v = (double)count;
        Double w = (double)buff.length;
        double x = new BigDecimal((v/w)*100).setScale(2, RoundingMode.HALF_UP).doubleValue();
        System.out.println(x);
        fileInputStream.close();
    }
}
