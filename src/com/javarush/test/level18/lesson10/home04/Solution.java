package com.javarush.test.level18.lesson10.home04;

/* Объединение файлов
Считать с консоли 2 имени файла
В начало первого файла записать содержимое второго файла так, чтобы получилось объединение файлов
Закрыть потоки
Темповые файлы создавать нельзя, т.к. на сервере заблокирована возможность создания каких любо файлов
*/

import java.io.*;


public class Solution {
    public static void main(String[] args) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        FileInputStream inputStream = new FileInputStream(file1);
        String file2 = reader.readLine();
        FileInputStream inputStream2 = new FileInputStream(file2);
        int count1 = inputStream.available();
        int count2 = inputStream2.available();
        byte[] buffer1 = new byte[count1];
        byte[] buffer2 = new byte[count2];
        int read1 = inputStream.read(buffer1);
        int read2 = inputStream2.read(buffer2);
        FileOutputStream outputStream = new FileOutputStream(file1);
        outputStream.write(buffer2, 0, read2);
        outputStream.write(buffer1, 0, read1);
        outputStream.close();
        reader.close();
        inputStream.close();
        inputStream2.close();
    }
}
