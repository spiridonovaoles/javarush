package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки ввода-вывода
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream readln1 = new FileInputStream(reader.readLine());
        String file2 = reader.readLine();
        FileOutputStream outputStream = new FileOutputStream(file2);
        String file3 = reader.readLine();
        FileOutputStream outputStream2 = new FileOutputStream(file3);
        int count2 = readln1.available() / 2;
        int count1 = readln1.available() - count2;
        byte[] buffer1 = new byte[count1];
        byte[] buffer2 = new byte[count2];
            int count = readln1.read(buffer1);
            outputStream.write(buffer1, 0, count);
            count = readln1.read(buffer2);
            outputStream2.write(buffer2, 0, count);
        readln1.close();
        reader.close();
        outputStream.close();
        outputStream2.close();

    }
}
