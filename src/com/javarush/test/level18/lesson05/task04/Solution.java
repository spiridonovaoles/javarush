package com.javarush.test.level18.lesson05.task04;

/* Реверс файла
Считать с консоли 2 имени файла: файл1, файл2.
Записать в файл2 все байты из файл1, но в обратном порядке
Закрыть потоки ввода-вывода
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileInputStream = new FileInputStream(reader.readLine());
        FileOutputStream outputStream = new FileOutputStream(reader.readLine());
        int [] buff = new int[fileInputStream.available()];
        if (fileInputStream.available()>0)
        {
            for (int i = buff.length - 1; i >= 0; i--)
            {
                int data = fileInputStream.read();
                buff[i] = data;
            }
        }
        for (int i = 0; i <buff.length; i++)
        {
            System.out.println(buff[i]);
        }
            for (int i = 0; i < buff.length; i++){
                if ( buff[i] == 10){ continue;}
                else {
                    outputStream.write(buff[i]);
                    }
            }
        fileInputStream.close();
        reader.close();
        outputStream.close();
    }
}
