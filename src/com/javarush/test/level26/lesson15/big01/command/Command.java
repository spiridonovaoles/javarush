package com.javarush.test.level26.lesson15.big01.command;


import com.javarush.test.level26.lesson15.big01.exception.InterruptOperationException;

/**
 * Created by user on 07.10.2015.
 */
interface Command
{
     void execute() throws InterruptOperationException;
}
