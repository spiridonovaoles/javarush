package com.javarush.test.level30.lesson04.home01;

import java.util.concurrent.TransferQueue;

/**
 * Created by Olesya on 02.11.2015.
 */
public class Consumer extends Thread
{
    TransferQueue<ShareItem> queue;
    public Consumer(TransferQueue<ShareItem> queue)
    {
        this.queue = queue;
    }
    @Override
    public void run()
    {
        try {
            sleep(500);
            while(!currentThread().isInterrupted()){
                while(true){
                    ShareItem item = queue.take();
                    System.out.println("Processing "+item.toString());
                }
            }

        } catch (InterruptedException e) {
        }
    }
}
