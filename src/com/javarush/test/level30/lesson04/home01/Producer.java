package com.javarush.test.level30.lesson04.home01;

import java.util.concurrent.TransferQueue;

/**
 * Created by Olesya on 02.11.2015.
 */
public class Producer extends Thread
{
    int i = 0;
    TransferQueue<ShareItem> queue;
    public Producer(TransferQueue<ShareItem> queue)
    {
        this.queue = queue;
    }

    @Override
    public void run()
    {
        try {
            while(!currentThread().isInterrupted()){
                while (i < 9)
                {
                    i++;
                    System.out.format("Элемент 'ShareItem-%s' добавлен", i);
                    System.out.println();
                    queue.offer(new ShareItem("ShareItem-" + i, i));
                    sleep(100);
                    if (queue.hasWaitingConsumer()) System.out.println("Consumer в ожидании!");
                }
            }

        } catch (InterruptedException e) {
        }
    }
}
