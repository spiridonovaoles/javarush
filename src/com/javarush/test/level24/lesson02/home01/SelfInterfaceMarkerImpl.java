package com.javarush.test.level24.lesson02.home01;

/**
 * Created by Olesya on 19.09.2015.
 */
public class SelfInterfaceMarkerImpl implements SelfInterfaceMarker
{
    public SelfInterfaceMarkerImpl()
    {
    }
    public int codeHash()
    {
        return 10;
    }
    public int codeCode()
    {
        return 20;
    }

}
