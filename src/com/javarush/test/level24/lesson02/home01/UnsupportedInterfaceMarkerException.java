package com.javarush.test.level24.lesson02.home01;

/**
 * Created by Olesya on 19.09.2015.
 */
public class UnsupportedInterfaceMarkerException extends Exception
{
    public UnsupportedInterfaceMarkerException()
    {
        super();
    }
}
