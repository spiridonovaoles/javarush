package com.javarush.test.level24.Race;



import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * Created by ПК on 23.01.2015.
 */
public class Track
{
    private int height;
    private int width;
    private int points = 0;
    private static int life = 3;
    private PlayerCar playerCar;
    private ArrayList<OtherCars> otherCars;
    private boolean isGameOver;
    static int[][] matrix;
    public static Track game;

    public Track(int height, int width, PlayerCar playerCar, ArrayList<OtherCars> otherCars)
    {
        this.height = height;
        this.width = width;
        matrix = new int[height][width];
        this.playerCar = playerCar;
        this.otherCars = otherCars;

    }

    public int getHeight()
    {
        return height;
    }

    public int getWidth()
    {
        return width;
    }

    public PlayerCar getPlayerCar()
    {
        return playerCar;
    }

    public int[][] getMatrix()
    {
        return matrix;
    }

    public ArrayList<OtherCars> getOtherCars()
    {
        return otherCars;
    }

    public static void main(String[] args)throws InterruptedException
    {

        game =  new Track(20,20,new PlayerCar(8,15),new ArrayList<OtherCars>());
        game.otherCars.add(new OtherCars());
        game.otherCars.add(new OtherCars());

        game.run();
    }
    //основая игровая логика
    public void run() throws InterruptedException{
        KeyboardObserver keyboardObserver = new KeyboardObserver();
        keyboardObserver.start();

        isGameOver = false;

        while(!isGameOver)
        {

            if (keyboardObserver.hasKeyEvents())
            {
                //получить самое первое событие из очереди
                KeyEvent event = keyboardObserver.getEventFromTop();
                //Если равно символу 'q' - выйти из игры.
                if (event.getKeyChar() == 'q') return;
                //Если "стрелка влево" - сдвинуть машинку влево
                if (event.getKeyCode() == KeyEvent.VK_LEFT)
                    playerCar.left();
                    //Если "стрелка вправо" - сдвинуть машинку вправо
                else if (event.getKeyCode() ==  KeyEvent.VK_RIGHT)
                    playerCar.right();
            }

            if(otherCars.get(0).getY() > height){
                otherCars.set(0,new OtherCars());
                points++;
            }
            if(otherCars.get(1).getY() > height)
            {
                otherCars.set(1,new OtherCars());
                points++;
            }
            othersCarDown();
            isTouch(playerCar, otherCars);
            print();
            Thread.sleep(300);
        }
        //если проиграл....
        for(int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j< matrix[0].length; j++)
            {
                System.out.print("Х");
            }
            System.out.println();
        }

        System.out.println("Ты проиграл...");
        System.out.println(" **** Очки: " + points +" ****");
        System.out.println(" **** Жизней:" +life +" ****");

    }
    //движения машинок вниз
    public void othersCarDown()
    {
        for(OtherCars cars : otherCars)
            cars.move();

    }
    //отрисовка происходящего на экран
    public void print(){
        //копируем трэк
        int[][] temp = new int[height][width];
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                temp[i][j] = matrix[i][j];
            }
        }
        //печатаем в него другие машинки
        OtherCars.print(otherCars,temp);


        //печатаем в него машинку игрока
        PlayerCar.print(playerCar,temp);



        //печатаем поле на экран
        for(int i = 0; i < matrix.length; i++)
        {
            System.out.print("|");
            for (int j = 0; j< matrix[0].length; j++)
            {

                if(temp[i][j] == 0)
                    System.out.print(" ");
                if(temp[i][j] == 1)
                    System.out.print("X");
            }
            System.out.print("|");
            System.out.println();
        }
        //печать пробелов и очков
        System.out.println(" **** Очки: " + points +" ****");
        System.out.println(" **** Жизней:" +life +" ****");
        System.out.println();
        System.out.println();
        System.out.println();

    }
    //Проверка на столкновения
    public void isTouch(PlayerCar car, ArrayList<OtherCars> otherCars)
    {
        try
        {

            for(int i =0; i<otherCars.size();i++)
            {
                for(int j =0; j < car.cord.size(); j++)
                    if(otherCars.get(i).cord.contains(car.cord.get(j)))
                    {
                        otherCars.set(i,new OtherCars());

                        life--;
                    }
            }

        }catch (Exception e){}
        if(life == 0){

            isGameOver = true;

        }

    }

}
