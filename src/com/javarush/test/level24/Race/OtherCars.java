package com.javarush.test.level24.Race;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ПК on 23.01.2015.
 */
public class OtherCars
{
    //координаты начальной точки
    private int x;
    private int y;
    //список координат для проверки на столкновения
    public ArrayList<String> cord;
    //шаблон машинки
    public int[][] car = new int[][]{
            {0,1,0},
            {1,1,1},
            {1,1,1},
            {1,1,1},};

    public void setCord(ArrayList<String> cord)
    {
        this.cord = cord;
    }
    //ставит случайные координаты сверху
    public OtherCars()
    {
        this.x = new Random().nextInt(18);
        this.y = new Random().nextInt(3);
    }
    //двигает машинку вниз
    public void move(){
        y++;
    }


    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int[][] getCar()
    {
        return car;
    }
    //Все машинки передаваемые в АррэйЛисте, рисуются в передаваему матрицу "temp" из класса Track
    public static void print(ArrayList<OtherCars> otherCars, int[][] temp)
    {
        //печать другой машины
        for(int i =0; i< otherCars.size();i++)
        {
            int left = otherCars.get(i).getX();
            int top = otherCars.get(i).getY();
            int[][] otherCar = otherCars.get(i).getCar();
            Track.game.getOtherCars().get(i).setCord(new ArrayList<String>());

            for (int k = 0; k < 4; k++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (top + k >= Track.game.getHeight() || left + j >= Track.game.getWidth()) continue;
                    if (otherCar[k][j] == 1)
                        temp[top + k][left + j] = 1;

                    otherCars.get(i).cord.add(String.valueOf(top + k) + " " + String.valueOf(left + k));
                }
            }
        }

    }

}
