
package com.javarush.test.level24.Race;

import java.util.ArrayList;

/**
 * Created by ПК on 23.01.2015.
 */
public class PlayerCar
{
    //координаты начальной точки
    int x;
    int y;
    //список координат для проверки на столкновения
    public ArrayList<String> cord;
    //шаблон машинки
    public int[][] car = new int[][]{
            {0,1,0},
            {1,1,1},
            {0,1,0},
            {1,1,1},

    };

    public PlayerCar(int x, int y)
    {
        this.x = x;
        this.y = y;


    }

    public int[][] getCar()
    {
        return car;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public void left()
    {
        x--;
        if(!isNotEndOfTack())
            x++;
    }
    public void right(){
        x++;
        if(!isNotEndOfTack())
            x--;
    }
    private boolean isNotEndOfTack(){
        if(x < 0) return false;
        else
        if(x == Track.game.getWidth() - 2) return false;
        return true;

    }
    //Рисуем машинку в temp, которое будет нашим треком
    public static void print(PlayerCar car,int[][] temp){
        //печатаем в него машинку игрока
        int[][] playercar = temp;
        int left = car.getX();
        int top = car.getY();
        int[][] carMatrix = car.getCar();
        Track.game.getPlayerCar().cord = new ArrayList();
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (top + i >= Track.game.getHeight() || left + j >= Track.game.getWidth()) continue;
                if (carMatrix[i][j] == 1)
                    playercar[top + i][left + j] = 1;
                car.cord.add(String.valueOf(top + i) + " " + String.valueOf(left + i));
            }
        }

    }
}
