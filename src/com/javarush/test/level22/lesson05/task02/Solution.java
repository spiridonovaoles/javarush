package com.javarush.test.level22.lesson05.task02;



/* Между табуляциями
Метод getPartOfString должен возвращать подстроку между первой и второй табуляцией.
На некорректные данные бросить исключение TooShortStringException.
Класс TooShortStringException не менять.
*/
public class Solution
{
    public static String getPartOfString(String string) throws TooShortStringException
    {
        if (string == null || string.isEmpty())
        {
            throw new TooShortStringException();
        }
        int indexFirstSpace = string.indexOf(9) + 1;
        char[] chars = string.toCharArray();
        int countSpace = 0;
        int indexLastSpace = 0;
        for (int i = 0; i < chars.length; i++)
        {
            if (chars[i] == 9)
            {
                countSpace++;
                if (countSpace == 2)
                {
                    indexLastSpace = i;
                    break;
                }
            }
        }
        if (countSpace <= 1)
        {
            throw  new TooShortStringException();
        }
        return string.substring(indexFirstSpace, indexLastSpace);
    }

    public static class TooShortStringException extends Exception
    {
    }
}
