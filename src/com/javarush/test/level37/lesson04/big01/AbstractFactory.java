package com.javarush.test.level37.lesson04.big01;

/**
 * Created by Olesya on 17.01.2016.
 */
public interface AbstractFactory
{
    public abstract Human getPerson(int age);
}
