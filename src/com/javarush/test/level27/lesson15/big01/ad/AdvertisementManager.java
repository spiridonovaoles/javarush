package com.javarush.test.level27.lesson15.big01.ad;


import com.javarush.test.level27.lesson15.big01.ConsoleHelper;

import java.util.*;

public class AdvertisementManager {
    private final AdvertisementStorage storage = AdvertisementStorage.getInstance();
    private int timeSeconds;

    public AdvertisementManager(int timeSeconds) {
        this.timeSeconds = timeSeconds;
    }

    public void processVideos() throws NoVideoAvailableException {
        if (storage.list().isEmpty())
            throw new NoVideoAvailableException();

        List<Advertisement> advertisements = storage.list();

        ArrayList<Advertisement> adList = new ArrayList<>();
        for (Advertisement ad : advertisements)
        {
            if (ad.getHits() > 0)
                adList.add(ad);
        }
        if(adList.isEmpty()) {
            throw new NoVideoAvailableException();
        }
        ArrayList<ArrayList<Advertisement>> lists =
                getAllSubsets(adList, 0, new ArrayList<Advertisement>());

        Iterator<ArrayList<Advertisement>> iterator = lists.iterator();
        while (iterator.hasNext()) {
            int duration = 0;
            ArrayList<Advertisement> list = iterator.next();

            for (Advertisement ad : list)
                duration += ad.getDuration();

            if (duration > timeSeconds)
                iterator.remove();
        }

        Collections.sort(lists, new Comparator<ArrayList<Advertisement>>() {
            @Override
            public int compare(ArrayList<Advertisement> o1, ArrayList<Advertisement> o2) {
                long amount1 = 0;
                int duration1 = 0;

                for (Advertisement ad : o1) {
                    amount1 += ad.getAmountPerOneDisplaying();
                    duration1 += ad.getDuration();
                }

                long amount2 = 0;
                int duration2 = 0;

                for (Advertisement ad : o2) {
                    amount2 += ad.getAmountPerOneDisplaying();
                    duration2 += ad.getDuration();
                }

                if (amount1 != amount2)
                    return (int) (amount2 - amount1);
                else if (duration1 != duration2)
                    return duration2 - duration1;
                else
                    return o1.size() - o2.size();
            }
        });

        List<Advertisement> showList = new LinkedList<>(lists.get(0));

        Collections.sort(showList, new Comparator<Advertisement>() {
            @Override
            public int compare(Advertisement o1, Advertisement o2) {
                if (o2.getAmountPerOneDisplaying() != o1.getAmountPerOneDisplaying())
                    return (int) (o2.getAmountPerOneDisplaying() - o1.getAmountPerOneDisplaying());
                else
                    return o1.getDuration() - o2.getDuration();
            }
        });

        for (Advertisement ad : showList) {
            int amountPerSec = (int) (1000 * ((double) ad.getAmountPerOneDisplaying() / ad.getDuration()));
            ConsoleHelper.writeMessage(String.format("%s is displaying... %d, %d", ad.getName(),
                    ad.getAmountPerOneDisplaying(), amountPerSec));
            ad.revalidate();
        }
    }

    private static ArrayList<ArrayList<Advertisement>> getAllSubsets(ArrayList<Advertisement> set, int index, ArrayList<Advertisement> subset) {
        ArrayList<ArrayList<Advertisement>> subsets = new ArrayList<>();

        if (index == set.size()) {
            subsets.add(new ArrayList<>(subset));
            return subsets;
        }

        subsets.addAll(getAllSubsets(set, index + 1, subset));
        subset.add(set.get(index));
        subsets.addAll(getAllSubsets(set, index + 1, subset));
        subset.remove(subset.size() - 1);
        return subsets;
    }
}