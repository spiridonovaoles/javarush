package com.javarush.test.level16.lesson10.task03;

/* Снова interrupt
Создай нить TestThread.
В методе main создай экземпляр нити, запусти, а потом прерви ее используя метод interrupt().
*/

public class Solution {
    public static void main(String[] args) throws InterruptedException {
        TestThread test = new TestThread();
        test.start();
        Thread.sleep(4000);
        test.interrupt();
    }
    public static class TestThread extends Thread{
        private int i;
        public void run()
        {
            try{
                while(!currentThread().isInterrupted()){
                    System.out.println("t " +  i++);
                    sleep(100);
                }
            }
            catch(InterruptedException e){
                System.out.print("Прервано!");
            }
        }
    }
}
