package com.javarush.test.level16.lesson13.bonus01;
import com.javarush.test.level16.lesson13.bonus01.common.ImageReader;
import com.javarush.test.level16.lesson13.bonus01.common.ImageTypes;
import com.javarush.test.level16.lesson13.bonus01.common.JpgReader;
import com.javarush.test.level16.lesson13.bonus01.common.BmpReader;
import com.javarush.test.level16.lesson13.bonus01.common.PngReader;

/**
 * Created by Olesya on 10.05.2015.
 */
public class ImageReaderFactory
{
    public static  ImageReader getReader(ImageTypes format){
        if (ImageTypes.BMP.equals(format)) return new BmpReader();
        else
        if (ImageTypes.JPG.equals(format)) return new JpgReader();
        else
        if (ImageTypes.PNG.equals(format)) return new PngReader();
        else
            throw new IllegalArgumentException("Неизвестный тип картинки");
    }
}

