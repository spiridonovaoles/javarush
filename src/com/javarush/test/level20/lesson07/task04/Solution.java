package com.javarush.test.level20.lesson07.task04;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* Externalizable Solution
Сериализуйте класс Solution.
Подумайте, какие поля не нужно сериализовать.
Объект всегда должен содержать актуальные на сегодняшний день данные.
*/
public class Solution implements Externalizable{
    public static void main(String[] args)throws IOException, ClassNotFoundException {
        System.out.println(new Solution(4));
        Solution instance = new Solution(41);

        //Serializing the Solution instance
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Solution.tmp"));
        oos.writeObject(instance);
        oos.close();

        //Recreating the instance by reading the serialized object data store
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Solution.tmp"));
        Solution apartment = (Solution) ois.readObject();
        ois.close();


        //The Solution behavior have been broken
        System.out.println(instance);
        System.out.println(apartment);
        System.out.println("=========================================================");
    }


    private transient final String pattern = "dd MMMM yyyy, EEEE";
    private Date currentDate;
    private int temperature;
    String string;

    public Solution() {
    }

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today is %s, and current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException
    {
        out.writeObject(string);
        //out.writeInt(temperature);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
    {
        string = (String) in.readObject();
        /*currentDate = (Date) in.readObject();
        temperature = in.readInt();
        string = "Today is %s, and current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);*/
    }

    @Override

    public String toString() {
        return this.string;
    }
}
