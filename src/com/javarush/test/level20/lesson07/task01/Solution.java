package com.javarush.test.level20.lesson07.task01;

import java.io.*;

/* Externalizable для апартаментов
Реализуйте интерфейс Externalizable для класса Apartment
Подумайте, какие поля не нужно сериализовать.
*/
public class Solution {

    public static class Apartment implements Externalizable {

        private String address;
        private int year;

        /**
         * Mandatory public no-arg constructor.
         */
        public Apartment() { super(); }

        public Apartment(String adr, int y) {
            address = adr;
            year = y;
        }

        @Override
        public void writeExternal(ObjectOutput out) throws IOException
        {
            out.writeObject(address);
            out.writeInt(year);
        }

        @Override
        public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
        {
            address = (String) in.readObject();
            year = in.readInt();

        }
        /**
         * Prints out the fields. used for testing!
         */
        public String toString() {
            return("Address: " + address + "\n" + "Year: " + year);
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Apartment instance = new Apartment("avtostr", 41);

        //Serializing the Apartment instance
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Apartment.tmp"));
        oos.writeObject(instance);
        oos.close();

        //Recreating the instance by reading the serialized object data store
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Apartment.tmp"));
        Apartment apartment = (Apartment) ois.readObject();
        ois.close();


        //The Apartment behavior have been broken
        System.out.println("Instance reference check : " + instance);
        System.out.println("Instance reference check : " + apartment);
        System.out.println("=========================================================");
    }
}
