package com.javarush.test.level20.lesson10.home02;

import java.io.ObjectInputStream;
import java.io.Serializable;

/* Десериализация
На вход подается поток, в который записан сериализованный объект класса A либо класса B.
Десериализуйте объект в методе getOriginalObject предварительно определив, какого именно типа там объект.
Реализуйте интерфейс Serializable где необходимо.
*/
public class Solution implements Serializable
{
    private static final long serialVersionUID= 1L;
    public A getOriginalObject(ObjectInputStream objectStream)
    {
        Object obj = null;
        try
        {
             obj = objectStream.readObject();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        if (obj instanceof B)
            return (B) obj;
        else if (obj instanceof A)
            return (A) obj;
        else return null;

    }

    public class A implements Serializable
    {
        private static final long serialVersionUID= 1L;
    }

    public class B extends A
    {
        public B()
        {
            System.out.println("inside B");
        }
    }
}
