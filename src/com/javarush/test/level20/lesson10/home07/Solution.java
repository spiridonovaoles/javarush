package com.javarush.test.level20.lesson10.home07;

import java.io.*;

/* Переопределение сериализации в потоке
Сериализация/десериализация Solution не работает.
Исправьте ошибки не меняя сигнатуры методов и класса.
Метод main не участвует в тестировании.
*/
public class Solution implements Serializable, AutoCloseable {
    private static final long serialVersionUID = 1L;
    transient private FileOutputStream stream;
    private String fileName;


    public Solution(String fileName) throws FileNotFoundException {
        this.stream = new FileOutputStream(fileName);
        this.fileName = fileName;

    }

    public void writeObject(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        stream = new FileOutputStream(fileName, true);
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }
    public static void main(String[] args) throws Exception, ClassNotFoundException {
        Solution instance = new Solution("Solution.tmp");
        instance.writeObject("String");
        instance.close();
        //Serializing the Solution instance
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Solution2.tmp"));

        oos.writeObject(instance);
        oos.flush();
        oos.close();

        //Recreating the instance by reading the serialized object data store
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Solution2.tmp"));
        Solution solution = (Solution) ois.readObject();
        ois.close();
        solution.writeObject("Hi2");



        //The Solution behavior have been broken
        System.out.println("Instance reference check : " + instance);
        System.out.println("Instance reference check : " + solution);
        System.out.println("=========================================================");
    }
}
