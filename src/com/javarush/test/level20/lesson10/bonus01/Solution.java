package com.javarush.test.level20.lesson10.bonus01;

import java.util.List;
import java.util.ArrayList;

/* Алгоритмы-числа
Число S состоит из M чисел, например, S=370 и M(количество цифр)=3
Реализовать логику метода getNumbers, который должен среди натуральных чисел меньше N (long)
находить все числа, удовлетворяющие следующему критерию:
число S равно сумме его цифр, возведенных в M степень
getNumbers должен возвращать все такие числа в порядке возрастания

Пример искомого числа:
370 = 3*3*3 + 7*7*7 + 0*0*0
8208 = 8*8*8*8 + 2*2*2*2 + 0*0*0*0 + 8*8*8*8

На выполнение дается 10 секунд и 50 МБ памяти.
*/
public class Solution
{
    public static int[] getNumbers(int N)
    {
        final int NUMBERS = 10;
        final int SQRT = 12;

        long[][] przv = new long[NUMBERS][SQRT];
        for (int i = 0; i < NUMBERS; i++)
        {
            for (int j = 0; j < SQRT; j++)
            {
                long temp = i;
                for (int z = 0; z < j - 1; z++)
                {
                    temp *= i;
                }
                przv[i][j] = temp;
            }
        }

        List<Integer> list = new ArrayList<>();
        int[] result = null;
        List<Integer> mas = new ArrayList<>();

        for (int j = 1; j < N; j++)
        {
            String s = Integer.toString(j);
            int num = s.length();
            mas.clear();
            int sum = 0;
            int M = j;
            while (M > 0)
            {
                mas.add(M % 10);
                M = (M / 10);
            }
            for (int deg : mas) {
                sum += przv[deg][num];
            }
            if (sum == j)
            {
                list.add(j);
            }
        }

        result = new int[list.size()];

        for (int i = 0; i < list.size(); i++)
        {
            result[i] = list.get(i);
        }

        return result;

    }
    public static void main(String[] args)
    {
        Long t0 = System.currentTimeMillis();
        int n = 146511208;
        int[] numbers = getNumbers(n);
        Long t1 = System.currentTimeMillis();
        System.out.println("time: " + (t1 - t0) / 1000d + " sec");
        System.out.println("memory: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024) + " mb");
        for (int i = 0; i < numbers.length; i++)
        {
            System.out.print(numbers[i] + ", ");
        }
        System.out.println();

    }
}
