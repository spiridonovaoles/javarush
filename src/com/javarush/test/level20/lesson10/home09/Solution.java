package com.javarush.test.level20.lesson10.home09;

import java.util.LinkedList;
import java.util.List;
import java.io.*;

/* Знакомство с графами
Дан ориентированный плоский граф Solution, содержащий циклы и петли.
Пример, http://edu.nstu.ru/courses/saod/images/graph1.gif
Сериализовать Solution.
Все данные должны сохранить порядок следования.
*/
public class Solution implements Serializable
{
    int node;
    List<Solution> edges = new LinkedList<>();

    private void writeObject(ObjectOutputStream s) throws IOException
    {
        s.defaultWriteObject();
        s.writeInt(edges.size());
        for (Solution e : edges)

            s.writeObject(e);

    }

    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException
    {

        s.defaultReadObject();

        int size = s.readInt();
        LinkedList<Solution> arr = new LinkedList<>();
        for (int i = 0; i < size; i++)
        {
            Object o = s.readObject();
            arr.add((Solution) o);
        }

    }
}
