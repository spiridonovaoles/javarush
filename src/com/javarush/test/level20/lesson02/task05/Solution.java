package com.javarush.test.level20.lesson02.task05;

import java.io.*;

/* И еще раз о синхронизации
Реализуйте логику записи в файл и чтения из файла для класса Object
Метод load должен инициализировать объект данными из файла
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(java.lang.String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name = File.createTempFile("your_file_name", null);
            OutputStream outputStream = new FileOutputStream("c:\\1.txt");
            InputStream inputStream = new FileInputStream("c:\\1.txt");

            Object object = new Object();
            object.string1 = new String();   //string #1
            object.string2 = new String();   //string #2
            object.save(outputStream);
            outputStream.flush();

            Object loadedObject = new Object();
            loadedObject.string1 = new String(); //string #3
            loadedObject.string2 = new String(); //string #4


            loadedObject.load(inputStream);
            if(object.equals(loadedObject)) System.out.println("Yes!");
            loadedObject.string1.print();
            loadedObject.string2.print();

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }


    public static class Object {
        public String string1;
        public String string2;

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter writer =  new PrintWriter(outputStream);
            PrintStream consoleStream = System.out;
            ByteArrayOutputStream outputSt = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(outputSt);
            System.setOut(stream);
            this.string1.print();
            this.string2.print();
            java.lang.String result = outputSt.toString();
            writer.println(result);
            System.setOut(consoleStream);
            writer.flush();
            writer.close();
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            java.lang.String st = reader.readLine();
            int tmp = countStrings;
            countStrings = Integer.parseInt(st.replaceAll("string #",""))-1;
            this.string1 = new String();
            st = reader.readLine();
            countStrings = Integer.parseInt(st.replaceAll("string #",""))-1;
            this.string2 =new String();
            countStrings = tmp;
            reader.close();
        }
    }

    public static int countStrings;

    public static class String {
        private final int number;

        public String() {
            number = ++countStrings;
        }

        public void print() {
            System.out.println("string #" + number);
        }
    }
}
