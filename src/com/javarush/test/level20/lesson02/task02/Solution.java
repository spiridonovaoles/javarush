package com.javarush.test.level20.lesson02.task02;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* Читаем и пишем в файл: JavaRush
Реализуйте логику записи в файл и чтения из файла для класса JavaRush
В файле your_file_name.tmp может быть несколько объектов JavaRush
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution
{
    public static void main(String[] args)
    {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try
        {
            File your_file_name = File.createTempFile("c:\\1.txt", null);
            OutputStream outputStream = new FileOutputStream("c:\\1.txt");
            InputStream inputStream = new FileInputStream("c:\\1.txt");

            JavaRush javaRush = new JavaRush();
            User one = new User();
            one.setFirstName("Хуан");
            one.setLastName("Хуанович");
            one.setBirthDate(new Date());
            one.setMale(true);
            one.setCountry(User.Country.RUSSIA);
            javaRush.users.add(one);
            User two = new User();
            two.setFirstName("Мария");
            two.setLastName("Хуановна");
            two.setBirthDate(new Date(1));
            two.setMale(false);
            two.setCountry(User.Country.UKRAINE);
            javaRush.users.add(two);
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            if (javaRush.equals(loadedObject))
                System.out.println("yes!");//check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны
            /*System.out.println(loadedObject.users.get(0).getFirstName());
            System.out.println(loadedObject.users.get(0).getLastName());
            System.out.println(loadedObject.users.get(0).getBirthDate());
            System.out.println(loadedObject.users.get(0).isMale());
            System.out.println(loadedObject.users.get(0).getCountry());*/
            outputStream.close();
            inputStream.close();

        }
        catch (IOException e)
        {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush
    {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception
        {
            PrintWriter writer = new PrintWriter(outputStream);
            if (users.size() > 0)
            {
                for (User one : users)
                {
                    writer.println(one.getFirstName());
                    writer.println(one.getLastName());
                    writer.println(new SimpleDateFormat("dd.MM.yyyy").format(one.getBirthDate()));
                    writer.println(one.isMale());
                    writer.println(one.getCountry().getDisplayedName());
                    writer.flush();
                }
            }
            writer.close();
        }

        public void load(InputStream inputStream) throws Exception
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                while (reader.ready())
                {
                    User one = new User();
                    one.setFirstName(reader.readLine());
                    one.setLastName(reader.readLine());
                    Date date = new SimpleDateFormat("dd.MM.yyyy").parse(reader.readLine());
                    one.setBirthDate(date);
                    if(reader.readLine().equals("true"))one.setMale(true);
                    String temp = reader.readLine();
                    if (User.Country.RUSSIA.getDisplayedName().equals(temp)) one.setCountry(User.Country.RUSSIA);
                    if (User.Country.UKRAINE.getDisplayedName().equals(temp)) one.setCountry(User.Country.UKRAINE);
                    if (User.Country.OTHER.getDisplayedName().equals(temp)) one.setCountry(User.Country.OTHER);
                    users.add(one);
                }
            reader.close();
        }
    }
}
