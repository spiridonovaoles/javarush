package com.javarush.test.level25.lesson05.home01;

/**
 * Created by Olesya on 23.09.2015.
 */
public class LoggingStateThread extends Thread
{
    Thread target;

    public LoggingStateThread(Thread target)
    {
        this.target = target;
        setDaemon(true);
    }

    @Override
    public void run()
    {
        State firstState = target.getState();
        System.out.println(firstState);
        while (true)
        {
            State currentState = target.getState();
            if (firstState != currentState)
            {
                System.out.println(currentState);
                firstState = currentState;
            }
            if (currentState == State.TERMINATED)
            {
                break;
            }

        }
    }
}