package com.javarush.test.level31.lesson02.home02;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/* Находим все файлы
Реализовать логику метода getFileTree, который должен в директории root найти список всех файлов включая вложенные.
Используйте очередь, рекурсию не используйте.
Верните список всех путей к найденным файлам, путь к директориям возвращать не надо.
Путь должен быть абсолютный.
*/
public class Solution
{
    public static List<String> getFileTree(String root) throws IOException
    {
        File fl = new File(root);
        Stack<File> stack = new Stack<>();
        List<String> result = new ArrayList<>();
        File[] files = fl.listFiles();
        for (File file : files)
        {
            if (file.isFile())
            {
                result.add(file.getAbsolutePath());
            } else
            {
                stack.push(file);
            }
        }
        while (!stack.empty())
        {
            File[] childFiles = stack.pop().listFiles();
            if(childFiles != null) {
                for (File childFile : childFiles)
                {
                    if (childFile.isFile())
                    {
                        result.add(childFile.getAbsolutePath());
                    } else
                    {
                        stack.push(childFile);
                    }
                }
            }
        }

        return result;

    }

    public static void main(String[] args) throws IOException
    {
        for (String st : getFileTree("d:/Sony_video")) System.out.println(st);
    }
}
