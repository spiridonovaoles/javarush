package com.javarush.test.level36.lesson08.task01;


import java.io.*;
import java.lang.Character;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Использование TreeSet
Первым параметром приходит имя файла: файл1.
файл1 содержит только буквы латинского алфавита, пробелы, знаки препинания, тире, символы перевода каретки.
Отсортировать буквы по алфавиту и вывести на экран первые 5 различных букв в одну строку без разделителей.
Если файл1 содержит менее 5 различных букв, то вывести их все.
Буквы различного регистра считаются одинаковыми.
Регистр выводимых букв не влияет на результат.
Закрыть потоки.

Пример 1 данных входного файла:
zBk yaz b-kN
Пример 1 вывода:
abkny

Пример 2 данных входного файла:
caAC
A, aB? bB
Пример 2 вывода:
abc

Подсказка: использовать TreeSet
*/
public class Solution
{
    public static boolean checkWithRegExp(String userNameString)
    {
        Pattern p = Pattern.compile("^[a-zA-Z0-9]+$");
        Matcher m = p.matcher(userNameString);
        return m.matches();
    }

    public static void main(String[] args) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            BufferedReader in = new BufferedReader(new FileReader(new File(args[0]).getAbsoluteFile()));
            try
            {
                String s;
                while ((s = in.readLine()) != null)
                {
                    sb.append(s.toLowerCase());
                }
            }
            finally
            {
                in.close();
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        char[] buff = sb.toString().toCharArray();

        Set<Character> result = new TreeSet<>();

        for (int i = 0; i < buff.length; i++)
        {
            if (checkWithRegExp(String.valueOf((char) buff[i]))) result.add((char) buff[i]);
        }
        if (result.size() < 5)
        {
            for (Character s : result)
            {
                System.out.print(s);
            }
        } else if (result.size() > 5) {
            int count = 0;
            for (Character s : result)
            {
                System.out.print(s);
                count++;
                if (count == 5)
                {
                    break;
                }
            }
        }
    }
}

