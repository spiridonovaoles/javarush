package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sss = reader.readLine();
        String s = sss.substring(sss.indexOf('?'));

        char[] name = s.toCharArray();
        List<String> list = new ArrayList<String>();
        List<Integer> num = new ArrayList<Integer>();
        for (int i = 0; i < name.length; i++)
        {
            if(name[i] == '&'){
                num.add(i);
            }
        }
        list.add(s.substring(s.indexOf('?')+1,num.get(0)));
        int i=0;
        while(i < num.size()-1){
            list.add(s.substring(num.get(i)+1, num.get(i+1)));
        i++;
        }
        i = num.get(num.size()-1);
        list.add(s.substring(i+1 , s.length()));

        List<String> key = new ArrayList<String>();
        List<String> value = new ArrayList<String>();
        for(String ss : list){
            if(ss.contains("=")){
                key.add(ss.substring(0, ss.indexOf('=')));
                value.add(ss.substring(ss.indexOf('=') + 1));
            }
            else{
                key.add(ss);
                value.add(null);
            }
        }
        for (int j = 0; j < key.size() ; j++)
        {
            System.out.print(key.get(j) + " ");

        }
        System.out.println();
        for (int j = 0; j < key.size() ; j++)
        {
            if(key.get(j).equals("obj")){
            try {
                    alert(Double.parseDouble(value.get(j)));

            } catch (NumberFormatException e) {
                alert(value.get(j));
            }
        }
        }
        System.out.println();
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}
