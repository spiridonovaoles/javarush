package com.javarush.test.level15.lesson12.home07;

/**
 * Created by Olesya on 28.04.2015.
 */
import java.io.*;

public class ReaderDemo {

    public static void main(String[] args) {

        String s = "Hello World";

        // create a new StringReader
        Reader reader = new StringReader(s);

        try {
            // check if reader is ready
            System.out.println("" + reader.ready());

            // read the first five chars
            for (int i = 0; i < 15; i++) {
                char c = (char) reader.read();
                System.out.print("" + c);
            }

            // change line
            System.out.println();

            // close the stream
            reader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}