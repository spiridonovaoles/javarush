package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Olesya on 26.04.2015.
 */
public class Sun2
{
    private static Sun2 ourInstance = new Sun2();

    public static Sun2 getInstance()
    {
        return ourInstance;
    }

    private Sun2()
    {
    }
}
