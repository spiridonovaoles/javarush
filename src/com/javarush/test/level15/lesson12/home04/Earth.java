package com.javarush.test.level15.lesson12.home04;

/**
 * Created by Olesya on 26.04.2015.
 */
public class Earth implements Planet
{
    private static Earth instance;

    public static Earth getInstance() {
        if (instance == null) {
            instance = new Earth();
        }
        return instance;
    }
    private Earth(){

    }
}
