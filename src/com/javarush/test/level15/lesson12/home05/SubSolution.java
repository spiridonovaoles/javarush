package com.javarush.test.level15.lesson12.home05;

/**
 * Created by Olesya on 27.04.2015.
 */
public class SubSolution extends Solution
{
     SubSolution()
    {
    }

   SubSolution(int i, int c, int d)
    {
        super(i, c, d);
    }

     SubSolution(int i, double d)
    {
        super(i, d);
    }

    public SubSolution(int i, String s)
    {
        super(i, s);
    }

    public SubSolution(int i, String s, int r)
    {
        super(i, s, r);
    }

    public SubSolution(int i, String s, Solution f)
    {
        super(i, s, f);
    }

    protected SubSolution(int i, int c, double d)
    {
        super(i, c, d);
    }

    protected SubSolution(int i, double c, String h)
    {
        super(i, c, h);
    }

    protected SubSolution(int i, int c, float t)
    {
        super(i, c, t);
    }
    private SubSolution(int c, float d){}
    private SubSolution(int c, int d, String dd){}
    private SubSolution(int y, Object r){}
}
