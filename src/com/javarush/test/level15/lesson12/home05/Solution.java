package com.javarush.test.level15.lesson12.home05;

/* Перегрузка конструкторов
1. В классе Solution создайте по 3 конструктора для каждого модификатора доступа.
2. В отдельном файле унаследуйте класс SubSolution от класса Solution.
3. Внутри класса SubSolution создайте конструкторы командой Alt+Insert -> Constructors.
4. Исправьте модификаторы доступа конструкторов в SubSolution так, чтобы они соответствовали конструкторам класса Solution.
*/

public class Solution {
    Solution(){};
    Solution(int i, int c, int d){}
    Solution(int i,double d){}
    public Solution(int i,String s){}
    public Solution(int i,String s,int r){}
    public Solution(int i,String s, Solution f){}
    private Solution(int c, float d){}
    private Solution(int c, int d, String dd){}
    private Solution(int y, Object r){}
    protected Solution(int i, int c, double d){}
    protected Solution(int i, double c, String h){}
    protected Solution(int i, int c,  float t){}


}

