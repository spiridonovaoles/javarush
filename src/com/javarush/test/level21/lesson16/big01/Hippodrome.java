package com.javarush.test.level21.lesson16.big01;

import java.util.ArrayList;

/**
 * Created by Olesya on 28.07.2015.
 */
public class Hippodrome
{
    static ArrayList<Horse> horses = new ArrayList<>();
    public static Hippodrome game;

    public ArrayList<Horse> getHorses()
    {
        return horses;
    }

    public void run() throws InterruptedException
    {
        for (int i = 0; i < 100; i++)
        {
            move();
            print();
            Thread.sleep(500);
        }
    }

    public void move()
    {
        for (int i = 0; i < horses.size(); i++)
        {
            horses.get(i).move();
        }
    }

    public void print()
    {
        for (int i = 0; i < horses.size(); i++)
        {
            horses.get(i).print();
        }
        System.out.println();
        System.out.println();
    }

    public Horse getWinner(){
    Horse winner = null;
    double d = 0;
        for (int i = 0; i < horses.size() ; i++)
        {
        if (horses.get(i).getDistance() > d){
           d = horses.get(i).getDistance();
            winner = horses.get(i);
        }
    }
    return winner;
}
public void printWinner(){
        System.out.println("Winner is " + getWinner().getName() + "!");
}

    public static void main(String[] args) throws InterruptedException
    {
        game = new Hippodrome();
        game.horses.add(new Horse("Belka", 3, 0));
        game.horses.add(new Horse("Strelka", 3, 0));
        game.horses.add(new Horse("Black_horse", 3, 0));
        game.run();
        game.printWinner();
    }
}
