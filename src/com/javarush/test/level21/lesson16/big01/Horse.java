package com.javarush.test.level21.lesson16.big01;

/**
 * Created by Olesya on 28.07.2015.
 */
public class Horse
{
    String name;
    double speed;
    double distance;

    public Horse(String name, double speed, double distance)
    {
        this.name = name;
        this.speed = speed;
        this.distance = distance;
    }

    public String getName()
    {
        return name;
    }

    public void move()
    {
        distance += speed * Math.random();
    }

    public void print()
    {
        String res = "";
        int i = (int) distance;
        for (int j = 0; j < i; j++)
        {
            res+=".";
        }
        System.out.print(res + this.getName());
        System.out.println();
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getSpeed()
    {
        return speed;
    }

    public void setSpeed(double speed)
    {
        this.speed = speed;
    }

    public double getDistance()
    {
        return distance;
    }

    public void setDistance(double distance)
    {
        this.distance = distance;
    }
}
