package com.javarush.test.level21.lesson08.task03;

/* Запретить клонирование
Запретите клонировать класс B
Разрешите клонировать класс C
*/
public class Solution
{
    public static class A implements Cloneable
    {
        private int i;
        private int j;

        public A(int i, int j)
        {
            this.i = i;
            this.j = j;
        }

        @Override
        public Object clone() throws CloneNotSupportedException
        {
            Object o = null;
            try
            {
              o = super.clone();
            }
            catch (CloneNotSupportedException e)
            {
                System.out.println(e);
            }
            return o;
        }

        public int getI()
        {
            return i;
        }

        public int getJ()
        {
            return j;
        }
    }

    public static class B extends A
    {
        private String name;

        public B(int i, int j, String name)
        {
            super(i, j);
            this.name = name;
        }

        @Override
        public Object clone() throws CloneNotSupportedException
        {
            throw new CloneNotSupportedException();
        }

        public String getName()
        {
            return name;
        }
    }

    public static class C extends B
    {
        public C(int i, int j, String name)
        {
            super(i, j, name);
        }

        @Override
        public Object clone() throws CloneNotSupportedException
        {
            return new C(getI(),getJ(),getName()==null?null:getName());
        }
    }
}
