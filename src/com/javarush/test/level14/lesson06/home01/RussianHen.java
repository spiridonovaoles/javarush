package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Olesya on 14.04.2015.
 */
class RussianHen extends Hen
{
    int eggsCount = 0;

    public int getCountOfEggsPerMonth()
    {
        eggsCount = 25;
        return eggsCount;
    }
    @Override
    public String getDescription(){
        String s = super.getDescription()+" Моя страна - " + Country.RUSSIA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
        return s;
    }
}