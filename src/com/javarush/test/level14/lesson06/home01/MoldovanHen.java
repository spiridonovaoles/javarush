package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Olesya on 14.04.2015.
 */
public class MoldovanHen extends Hen
{
    int eggsCount = 0;

    public int getCountOfEggsPerMonth()
    {
        eggsCount = 35;
        return eggsCount;
    }
    @Override
    public String getDescription(){
        String s = super.getDescription()+" Моя страна - " + Country.MOLDOVA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
        return s;
    }
}