package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String number1 = reader.readLine();
        String number2 = reader.readLine();
        int num1 = Integer.parseInt(number1);
        int num2 = Integer.parseInt(number2);
        while (num1 != 0 & num2 != 0 )
        {
            if (num1 >= num2)
            {
                num1 = num1 % num2;
            } else
            {
                num2 = num2 % num1;
            }
        }
        int nod = num1 + num2;
        System.out.println(nod);

    }
}

