package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by Olesya on 20.04.2015.
 */
public class Singleton
{
    public static Singleton instance;
    static Singleton getInstance(){
        if(instance == null ){
            instance = new Singleton();
        }
        return instance;
    }
    private Singleton(){

    }
}
