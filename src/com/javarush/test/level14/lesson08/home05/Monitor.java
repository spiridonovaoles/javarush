package com.javarush.test.level14.lesson08.home05;

/**
 * Created by Olesya on 15.04.2015.
 */
public class Monitor implements CompItem
{
    public String getName()
    {
        return this.getClass().getSimpleName();
    }
}