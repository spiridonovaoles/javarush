package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Olesya on 14.04.2015.
 */
public interface Bridge{
    int getCarsCount();
}