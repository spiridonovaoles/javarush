package com.javarush.test.level14.lesson08.bonus01;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    private static  int input(int i){
 return i/0;
}
    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            FileInputStream fis = new FileInputStream("c:\\badfile.txt");

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {

            String s = "sss";
            int i = Integer.parseInt(s);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            int [] unter = {1,2,3,4,5,6};
            for (int i = 0; i < 10; i++)
            {
                System.out.println(unter[i]);
            }

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            String s = null;
            String m = s.toLowerCase();

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {

            ArrayList<String> ex = new ArrayList<String>();
            String s = ex.get(18);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            HashMap<String, String> mas= new HashMap<String, String>(null);
            mas.put(null,null);
            mas.remove(null);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            int s = -15;
            String [] ex = new String[s];

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {int sss = 0;
            input(sss);

        } catch (Exception e)
        {
            exceptions.add(e);
        }

        try
        {
            System.out.println(1/0);

        } catch (Exception e)
        {
            exceptions.add(e);
        }
    }
}
