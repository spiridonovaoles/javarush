package com.javarush.test.level29.lesson15.big01.human;

/**
 * Created by Olesya on 06.11.2015.
 */
public interface Alive
{
    void live();
}
