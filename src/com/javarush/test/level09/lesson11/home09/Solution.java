package com.javarush.test.level09.lesson11.home09;

import java.util.*;

/* Десять котов
Создать класс кот – Cat, с полем «имя» (String).
Создать словарь Map(<String, Cat>) и добавить туда 10 котов в виде «Имя»-«Кот».
Получить из Map множество(Set) всех имен и вывести его на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap()
    {
        Map<String, Cat> map = new HashMap<String, Cat>();
        Cat vasya = new Cat("Vasya");
        map.put(vasya.name, vasya);
        Cat vasya2 = new Cat("Vasya2");
        map.put(vasya2.name, vasya2);
        Cat vasya3 = new Cat("Vasya3");
        map.put(vasya3.name, vasya3);
        Cat vasya4 = new Cat("Vasya4");
        map.put(vasya4.name, vasya4);
        Cat vasya5= new Cat("Vasya5");
        map.put(vasya5.name, vasya5);
        Cat vasya6 = new Cat("Vasya6");
        map.put(vasya6.name, vasya6);
        Cat vasya7 = new Cat("Vasya7");
        map.put(vasya7.name, vasya7);
        Cat vasya8 = new Cat("Vasya8");
        map.put(vasya8.name, vasya8);
        Cat vasya9 = new Cat("Vasya9");
        map.put(vasya9.name, vasya9);
        Cat vasya10 = new Cat("Vasya10");
        map.put(vasya10.name, vasya10);
        return map;
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map)
    {
        Set<Cat> set = new HashSet<Cat>();
        Iterator<Map.Entry<String, Cat>> itr = map.entrySet().iterator();
        while(itr.hasNext()) {
            Map.Entry<String, Cat>pair=itr.next();
            //String key = pair.getKey();
            Cat value = pair.getValue();
            set.add(value);
        }

        return set;
    }

    public static void printCatSet(Set<Cat> set)
    {
        for (Cat cat:set)
        {
            System.out.println(cat);
        }
    }

    public static class Cat
    {
        private String name;

        public Cat(String name)
        {
            this.name = name;
        }

        public String toString()
        {
            return "Cat "+this.name;
        }
    }


}
