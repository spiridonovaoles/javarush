package com.javarush.test.level09.lesson11.home04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* Конвертер дат
Ввести с клавиатуры дату в формате «08/18/2013»
Вывести на экран эту дату в виде «AUG 18, 2013».
Воспользоваться объектом Date и SimpleDateFormat.
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        try {
            Date dd = new SimpleDateFormat("MM/dd/yyyy").parse(s);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);

            System.out.println(dateFormat.format(dd).toUpperCase());
            //System.out.println(new SimpleDateFormat("dd/mm/yyyy HH:mm").parse("1/1/13 12:30"));
            //System.out.println(new SimpleDateFormat("dd/mm/yy HH:mm").parse("1/1/13 12:30"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
