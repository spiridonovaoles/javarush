package com.javarush.test.level09.lesson11.home01;

/* Деление на ноль
Создай метод public static void divisionByZero, в котором подели любое число на ноль и выведи на экран результат деления.
Оберни вызов метода divisionByZero в try..catch. Выведи стек-трейс исключения используя метод exception.printStackTrace()
*/

import java.util.Random;

public class Solution {

    public static void main(String[] args) {
        try {
        divisionByZero();
        }
        catch(ArithmeticException e){
            e.printStackTrace();
        }
    }

    public static void divisionByZero(){
        Random rand =new Random(47);
        int a = rand.nextInt(100);
        System.out.println((a + 1) / 0);
    }
}
