package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.*;
import java.util.ArrayList;
import java.util.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String it = reader.readLine();
        ArrayList<Integer> str = new ArrayList<Integer>();
        BufferedReader inStream = new BufferedReader(new FileReader(it));
        while(inStream.ready()){
            int in = Integer.parseInt(inStream.readLine());
            str.add(in);
        }
        Collections.sort(str);
        for (int i=0; i < str.size(); i++){
            if(str.get(i) % 2 == 0)
            {
                System.out.println(str.get(i));
            }
        }

    }
}
