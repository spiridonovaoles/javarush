package com.javarush.test.level13.lesson11.home03;

/* Чтение файла
1. Считать с консоли имя файла.
2. Вывести в консоль(на экран) содержимое файла.
3. Не забыть закрыть файл и поток.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader readerFileName = new BufferedReader(new InputStreamReader(System.in));
        String str = readerFileName.readLine();
        BufferedReader reader = new BufferedReader(new FileReader(str));
        while(reader.ready()){
            System.out.println(reader.readLine());
        }
        readerFileName.close();
        reader.close();

    }
}
