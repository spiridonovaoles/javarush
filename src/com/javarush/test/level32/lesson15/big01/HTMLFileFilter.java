package com.javarush.test.level32.lesson15.big01;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by Olesya on 10.01.2016.
 */
public class HTMLFileFilter extends FileFilter
{
    @Override
    public boolean accept(File f)
    {
        boolean flag = false;
        String tmp = f.getName().toLowerCase();
        if (f.isDirectory()) flag = true;
        else if (tmp.endsWith(".html")) flag = true;
        else if (tmp.endsWith(".htm")) flag = true;
        return flag;
    }

    @Override
    public String getDescription()
    {
        return "HTML и HTM файлы";
    }
}
