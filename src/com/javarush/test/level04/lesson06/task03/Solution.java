package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String number1 = reader.readLine();
        String number2 = reader.readLine();
        String number3 = reader.readLine();
        int a = Integer.parseInt(number1);
        int b = Integer.parseInt(number2);
        int c = Integer.parseInt(number3);
        if (a < b && a < c){
            if (b > c){
                System.out.println(b);
                System.out.println(c);
                System.out.println(a);
            }
            else{
                System.out.println(c);
                System.out.println(b);
                System.out.println(a);
            }
        }
        else   if (b < a && b < c) {
            if (a > c){
                System.out.println(a);
                System.out.println(c);
                System.out.println(b);
            }
            else{
                System.out.println(c);
                System.out.println(a);
                System.out.println(b);
            }

        }
        else   if (c < a && c < b){
            if (b > a){
                System.out.println(b);
                System.out.println(a);
                System.out.println(c);
            }
            else{
                System.out.println(a);
                System.out.println(b);
                System.out.println(c);
            }

        }
    }
}
