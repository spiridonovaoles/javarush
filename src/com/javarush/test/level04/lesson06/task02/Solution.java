package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String number1 = reader.readLine();
        String number2 = reader.readLine();
        String number3 = reader.readLine();
        String number4 = reader.readLine();
        int num1 = Integer.parseInt(number1);
        int num2 = Integer.parseInt(number2);
        int num3 = Integer.parseInt(number3);
        int num4 = Integer.parseInt(number4);
        int x = 0;
        if (num1 > num2){
            x = num1;
        }
        else{
            x = num2;
        }
        int y = 0;
        if (num3 > num4){
            y = num3;
        }
        else{
            y = num4;
        }if (x > y){
        System.out.println(x);;
        }
        else{
        System.out.println(y);
        }//Напишите тут ваш код

    }
}
