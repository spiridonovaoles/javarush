package com.javarush.test.level04.lesson16.home02;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String number1 = reader.readLine();
        String number2 = reader.readLine();
        String number3 = reader.readLine();
        int a = Integer.parseInt(number1);
        int b = Integer.parseInt(number2);
        int c = Integer.parseInt(number3);
        if (a < b && a < c)
        {
            if (b > c)
            {
                System.out.println(c);
            } else
            {
                System.out.println(b);
            }
        }
        if (c < a && c < b)
        {
            if (a > b)
            {
                System.out.println(b);
            } else
            {
                System.out.println(a);
            }
        }
        if (b < a && b < c)
        {
            if (a > c)
            {
                System.out.println(c);
            } else
            {
                System.out.println(a);
            }
        }

    }
}
