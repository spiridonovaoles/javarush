package com.javarush.test.level34.lesson15.big01.model;

/**
 * Created by Olesya on 28.01.2016.
 */
public enum Direction
{
    LEFT,
    RIGHT,
    UP,
    DOWN
}
