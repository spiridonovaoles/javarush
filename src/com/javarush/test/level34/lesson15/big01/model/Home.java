package com.javarush.test.level34.lesson15.big01.model;

import java.awt.*;

/**
 * Created by Olesya on 28.01.2016.
 */
public class Home extends GameObject
{
    public Home(int x, int y)
    {
        super(x, y, Model.FIELD_SELL_SIZE, Model.FIELD_SELL_SIZE);
    }

    @Override
    public void draw(Graphics graphics)
    {
        graphics.setColor(Color.GREEN);
        graphics.drawOval(getX(), getY(), getWidth() / 10, getHeight() / 10);
    }
}
