package com.javarush.test.level34.lesson15.big01.model;

import com.javarush.test.level34.lesson15.big01.controller.EventListener;

import java.nio.file.Paths;
import java.util.Set;

/**
 * Created by Olesya on 27.01.2016.
 */
public class Model
{
    public static final int FIELD_SELL_SIZE = 20;
    private EventListener eventListener;
    private GameObjects gameObjects;
    private int currentLevel = 60;
    private LevelLoader levelLoader = new LevelLoader(Paths.get("..\\res\\levels.txt"));

    public GameObjects getGameObjects()
    {
        return gameObjects;
    }

    public void setEventListener(EventListener eventListener)
    {
        this.eventListener = eventListener;
    }

    public void restartLevel(int level)
    {
        gameObjects = levelLoader.getLevel(level);
    }

    public void restart()
    {
        restartLevel(currentLevel);
    }

    public void startNextLevel()
    {
        currentLevel++;
        restart();
    }

    public void move(Direction direction)
    {
        Player player = this.getGameObjects().getPlayer();
        int newX = 0;
        int newY = 0;
        if (checkWallCollision(player, direction))
        {
            return;
        }
        if (checkBoxCollision(direction))
        {
            return;
        }
        switch (direction)
        {
            case UP:
                newY = -Model.FIELD_SELL_SIZE;
                break;
            case DOWN:
                newY = Model.FIELD_SELL_SIZE;
                break;
            case LEFT:
                newX = -Model.FIELD_SELL_SIZE;
                break;
            case RIGHT:
                newX = Model.FIELD_SELL_SIZE;
                break;
        }
        player.move(newX, newY);
        checkCompletion();
    }

    public boolean checkWallCollision(CollisionObject gameObject, Direction direction)
    {
        Set<Wall> walls = getGameObjects().getWalls();
        for (Wall wall : walls)
        {
            if (gameObject.isCollision(wall, direction)) return true;
        }
        return false;
    }

    public boolean checkBoxCollision(Direction direction)
    {
        boolean flag = false;
        Player player = getGameObjects().getPlayer();
        Set<Box> boxes = getGameObjects().getBoxes();
        Set<Home> homes = getGameObjects().getHomes();
        for (Box box : boxes)
        {
            if (player.isCollision(box, direction))
            {
                for (Box anotherBox : boxes)
                {
                    if (checkWallCollision(box, direction))
                    {
                        return true;
                    } else if (box.isCollision(anotherBox, direction))
                    {
                        return true;
                    }
                }
                int newX = 0;
                int newY = 0;
                switch (direction)
                {
                    case UP:
                        newY = -Model.FIELD_SELL_SIZE;
                        break;
                    case DOWN:
                        newY = Model.FIELD_SELL_SIZE;
                        break;
                    case LEFT:
                        newX = -Model.FIELD_SELL_SIZE;
                        break;
                    case RIGHT:
                        newX = Model.FIELD_SELL_SIZE;
                        break;
                }
                box.move(newX, newY);
                return false;
            }
        }

        return flag;
    }

    public void checkCompletion()
    {
        int count = 0;
        Set<Box> boxes = getGameObjects().getBoxes();
        Set<Home> homes = getGameObjects().getHomes();
        for (Box box : boxes)
        {
            for (Home home : homes)
            {
                if (box.getY() == home.getY() && box.getX() == home.getX()) count++;
            }
        }
        if (count == boxes.size()) eventListener.levelCompleted(currentLevel);
    }
}